#!/bin/bash
### Biowulf params
#SBATCH --job-name=run_sm
#SBATCH --output=%x-%j.out
#SBATCH --partition=norm
#SBATCH --mail-type=ALL          # Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --cpus-per-task=1              # CPUs
#SBATCH --mem=8G                        # Job memory request
#SBATCH --time=2-00:00:00
#SBATCH --export=NONE
#SBATCH --gres=lscratch:1

### Locus params
#$ -N run_sm
#$ -cwd
#$ -j y
#$ -l h_vmem=8G
#$ -o .
#$ -m e
#$ -M your_address@niaid.nih.gov

scriptdir=/home/user/git/functional-metagenomics

dir=/data/wgs
## output directory; subdirectory for each sample
## will be created underneath where the output files will go
outputdir=${dir}/functional

## config file for project
configfile=${dir}/fm.config.yaml

### COMMENT OUT below the section that you DO NOT need ############
##### commands for biowulf  ##############
# some module installed to shared directory
export MODULEPATH=/data/BCBB_microbiome_db/modulefiles:$MODULEPATH
module load snakemake/6.8.2
clusterfile=${scriptdir}/fm.biowulf.yaml
sbatchcmd="sbatch -c {cluster.threads} --mem={cluster.mem} --output=${outputdir}/{cluster.log}/%x-%j.out --partition={cluster.partition} --time={cluster.time} {cluster.extra}"

snakemake -s ${scriptdir}/functional_metagenomics.smk --jobs 32 --jobname "{name}.{cluster.jobname}.{jobid}" \
	  --configfile ${configfile} --cluster "${sbatchcmd}" --cluster-config ${clusterfile} \
	  --latency-wait 120 --max-jobs-per-second 1 \
	  --nolock --keep-going --keep-incomplete --use-envmodules


##### commands for locus  ##############
export MODULEPATH=/hpcdata/bcbb/shared/microbiome_share/modules:$MODULEPATH
module load snakemake/5.4.0-Python-3.6.7 || exit 1
clusterfile=${scriptdir}/fm.locus.yaml
drmaacmd=" -l h_vmem={cluster.h_vmem} -j y -pe threaded {cluster.threads} {cluster.extra}"


snakemake -s ${scriptdir}/functional_metagenomics.smk --jobs 32 --jobname "{name}.{cluster.jobname}.{jobid}" \
	  --configfile ${configfile} \
	  --drmaa "${drmaacmd}" --cluster-config ${clusterfile} --drmaa-log-dir ${outputdir}/{cluster.log} \
	  --nolock --keep-going --keep-incomplete --use-envmodules
