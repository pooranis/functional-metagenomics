import sys
import re
import os
from os.path import join as pjoin
import snakemake.utils as sm
import yaml
import json
import shutil

## setup is most important!! sets up working, output and temp directories and reads in cluster config
include: pjoin(config["scriptdir"], "setup.smk")
## trimwgs, spades, map2assemb
include: pjoin(config["scriptdir"], "trim_assemble_map.smk")

## setup is most important!! sets up working, output and temp directories and reads in cluster config
include: pjoin(config["scriptdir"], "setup.smk")
## trimwgs, spades, map2assemb
include: pjoin(config["scriptdir"], "trim_assemble_map.smk")
## prodigal and verse
include: pjoin(config["scriptdir"], "genepred.smk")
## kraken and eukdetect
include: pjoin(config["scriptdir"], "taxonomy.smk")
## eggnog
include: pjoin(config["scriptdir"], "eggnog.smk")

# pprint(vars())
################ TARGET OUTPUT VARS #########################


# humann
HUMANNPATH = pjoin(SUBOUT, "humann", "{sample}.humann.mod_pathabundance.tsv")

# logs for rules operating on more than one sample
# LOGS = pjoin(OUT, "logs")


################ RULES ###########################
onerror:
     print("An error occurred in the functional metagenomics pipeline.")
     shell("mail -s 'Error in functional metagenomics pipeline.' {config[email]} < {log}")

localrules: all, createsampledir, createtrimdir, createlogdir

########## ALL RULE #############
rule all:
    input: TRIMMED = ancient(expand(TRIM, sample=SAMPLES, read=[1,2])),
           ASSEMBLYFASTA = expand(ASSEMBLYFASTA, sample=SAMPLES),
           BAMFILES = expand(BAM, sample=SAMPLES),
           PRODIGAL = expand(PRODIGAL + '{ext}', sample=SAMPLES, ext=['.fna', '.gff', '.faa', '.sco']),
           READCOUNTS = expand(READCOUNTS, sample=SAMPLES),
           EUKDETECT = expand(EUKDETECT, sample=SAMPLES),
           KRAKENREPORT = KRAKENREPORT,
           EGGNOG = EGGNOG
    message: "Run all steps."

##           HUMANNPATH = expand(HUMANNPATH, sample=SAMPLES),
## RGI = expand(rules.card_rgi.output.annotation, sample=SAMPLES)


# rule humann:
#     """run humann3 to get pathways"""
#     threads: clust_conf["humann"]["threads"]
#     envmodules: *clust_conf["humann"]["modules"]
#     resources: cc = repr(clust_conf["humann"])

#     input: fna = rules.genepred.output.fna,
#            readcounts = rules.genepred.output.readcounts,
#            kraken = kraken_out
#     output: genefam = pjoin(SUBOUT, "humann", "{sample}.humann.mod_genefamilies.tsv"),
#             pathabund = HUMANNPATH
#     params: work = pjoin(TMP, "humann"),
#             final = pjoin(SUBOUT, "humann"),
#             prefix = "{sample}.humann",
#             modprefix =  "{sample}.humann.mod",
#             taxprefix = "{sample}.humann.modtax",
#             python = clust_conf["humann"].get("python", "python"), ## in case python is called something else...
#             taxopy = clust_conf["humann"].get("taxopy")
#     shell:"""
#     ## cleanup failed jobs
#     rm -rf {params.final}
#     mkdir -p {params.work}

#     ## on exit mv temp to final outputdir
#     trap 'mv -v {params.work} {params.final}' EXIT ERR SIGTERM

#     humann --version
#     humann -i {input.fna} -o {params.work} --output-basename {params.prefix} --threads {threads} --input-format fasta \
#         --log-level INFO --metaphlan-options "-t rel_ab --bowtie2db {config[metaphlan_db]}"
#     rm -fv {params.work}/*/*.sam
#     rm -fv {params.work}/*/*.bt2
#     rm -fv {params.work}/*/*.ffn

#     ## modify readcounts in humann output to match output of verse
#     {params.python} {config[scriptdir]}/modify_humann_gene_counts.py -i {params.work} -p {params.prefix} -o {params.work}/{params.modprefix} -r {input.readcounts}

#     ## modify taxonomy in humann output to match output of kraken; if skipping kraken, comment this out
#     {config[scriptdir]}/genes2contig2taxid.py -p {params.work}/{params.modprefix}.pred2uniref.map.txt -k {input.kraken} -g {input.gff} -o {params.work}/{params.modprefix}
#     {params.taxopy}/bin/python {config[scriptdir]}/modify_humann_taxonomy.py --nosp -l {params.work}/{params.modprefix}.locus_tag2contig.txt -i {params.work}/{params.modprefix}_genefamilies.tsv -o {params.work}/{params.taxprefix} --ncbitax {config[taxonomy]}


#     if [[ -f "{params.work}/{params.modprefix}_genefamilies.tsv" ]]; then
#         humann -i {params.work}/{params.modprefix}_genefamilies.tsv -o {params.work} --output-basename {params.modprefix} \
#             --threads {threads} --input-format genetable --log-level INFO
#     fi

#     if [[ -f "{params.work}/{params.taxprefix}_genefamilies.tsv" ]]; then
#         humann -i {params.work}/{params.taxprefix}_genefamilies.tsv -o {params.work} --output-basename {params.taxprefix} \
#             --threads {threads} --input-format genetable --log-level INFO
#     fi

#     """

# rule card_rgi:
#     """run CARD rgi"""
#     threads: clust_conf["card_rgi"]["threads"]
#     envmodules: *clust_conf["card_rgi"]["modules"]
#     resources: cc = repr(clust_conf["card_rgi"])

#     input: faa = rules.genepred.output.faa,
#            readcounts = rules.genepred.output.readcounts
#     output: annotation = pjoin(SUBOUT, "card_rgi", "{sample}.card_rgi_diamond.with_counts.txt")
#     params: tempdir = pjoin(TMP, "card_rgi"),
#             outdir = pjoin(SUBOUT, "card_rgi"),
#             s = "{sample}",
#             annotfile = "{sample}.card_rgi_diamond.txt"
#     shell:"""
#     rgi main -v
#     rgi database -v --all

#     rm -rf {params.outdir}
#     mkdir -p {params.tempdir}

#     trap 'mv -v {params.tempdir} {params.outdir}' EXIT

#     inputfile=${{TMPDIR}}/{params.s}.nostar.faa
#     sed 's/[*]\+//g' {input.faa} >${{inputfile}}

#     ## run rgi
#     rgi main -i ${{inputfile}} -o {params.tempdir}/{params.s}.card_rgi_diamond -t protein -a diamond -n {threads}

#     ## join requires sorted files
#     tpmfile=$(basename {input.readcounts})
#     headersort ()  {{ (head -n 1 "${{1}}" && tail -n +2 "${{1}}" | sort -t$'\t' -k 1b,1)   }}
#     headersort {input.readcounts} >${{TMPDIR}}/${{tpmfile}}.sorted.txt
#     headersort {params.tempdir}/{params.annotfile} >${{TMPDIR}}/{params.annotfile}.sorted.txt

#     join -e$'\t' -t$'\t' --header -j 1 -a2 ${{TMPDIR}}/${{tpmfile}}.sorted.txt ${{TMPDIR}}/{params.annotfile}.sorted.txt \
#                >{params.tempdir}/{params.s}.card_rgi_diamond.with_counts.txt
#     """
