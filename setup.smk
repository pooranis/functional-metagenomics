"""
must include this file first in any other snakefile!
include: pjoin(config["scriptdir"], "setup.smk")

"""

import yaml
from os.path import join as pjoin

## cluster config
def default_clust(cc):
    """adds __default__ values from cluster config to other
       rules lacking those keys"""
    dkeys = set(cc["__default__"].keys())
    rules = [k for k in cc if k != "__default__"]
    for r in rules:
        toadd = dkeys.difference(cc[r].keys())
        if toadd:
            cc[r].update(dict((k, cc["__default__"][k]) for k in toadd))
    return(cc)

with open(config["clusterfile"]) as f:
    clust_conf = default_clust(yaml.safe_load(f))


### samples
SAMPLES = list(config["samples"].keys())
if SAMPLES is None:
    sys.exit("Sample prefixes are needed")


## output directory
workdir: config["workdir"]
OUT = config["outputdir"]

## output subdirectory for individual samples
SUBOUT = pjoin(OUT, "{sample}")

## temp directory
TMP = clust_conf["__default__"]["tmpdir"]
shell.prefix("export TMPDIR={TMP}; set -ve;")

# logs for rules operating on more than one sample
LOGS = pjoin(OUT, clust_conf["kraken"]["log"])


############## RULES #############################

# setup output directories
rule createsampledir:
    output: pjoin(SUBOUT, "snakefake")
    params: outdir = SUBOUT,
            logdir = LOGS
    shell:"""
    mkdir -p {params.outdir}
    touch {output}
    """

rule createlogdir:
    output: pjoin(LOGS, "snakefake")
    params: logdir = LOGS
    shell:"""
    mkdir -p {params.logdir}
    touch {output}
    """
