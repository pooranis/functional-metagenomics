"""
to use add to main snakefile:
## need setup to read in config files, and set up output directories, and shell prefix
include: os.path.join(config["scriptdir"], "setup.smk")
include: os.path.join(config["scriptdir"], "trim_assemble_map.smk")


"""

from os.path import join as pjoin
import snakemake.utils as sm

#### inputs ##########
R1 = lambda wildcards: pjoin(config["inputdir"], config["samples"][wildcards.sample] + config["r1"])
R2 = lambda wildcards: pjoin(config["inputdir"], config["samples"][wildcards.sample] + config["r2"])


################ TARGET OUTPUT VARS #########################

## directory to hold trimmed files
TRIMDIR = config.get('trimdir', "Not set")
# TRIMDIR = config["trimdir"]
TRIM = pjoin(TRIMDIR, "{sample}_R{read}_ted.fastq.gz")
TRIMJSON = pjoin(TRIMDIR, "{sample}_fastp.json")

# spades outputs
ASSEMBLYFASTA = pjoin(SUBOUT, "spades", "scaffolds.fasta")

# map2assem
BAM = pjoin(SUBOUT, "{sample}.bowtie2.markdup.bam")


############## RULES ########################################

rule createtrimdir:
    output: temp(pjoin(TRIMDIR, "snakefake"))
    params: trimdir = TRIMDIR,
            trimlogs = pjoin(LOGS, "trimmed")
    shell:"""
    if [[ "{TRIMDIR}" == "Not set" ]]; then
      echo "'trimdir' not set in config file."
      exit 1
    fi
    mkdir -p {params.trimdir}
    mkdir -p {params.trimlogs}
    touch {output}
    """


rule trimwgs:
    threads: clust_conf["trimwgs"]["threads"]
    log: sm.simplify_path(pjoin(LOGS, "trimmed", "trimwgs.{sample}.jobid.*"))
    envmodules: clust_conf["trimwgs"]["modules"]
    input: r1 = ancient(R1),
           r2 = ancient(R2),
           fake = ancient(rules.createtrimdir.output)
    output: r1 = TRIM.format(read=1, sample="{sample}"),
            r2 = TRIM.format(read=2, sample="{sample}"),
            json = TRIMJSON,
            html = pjoin(TRIMDIR, "{sample}_fastp_log.html")
    params: ad = config.get("adapter", None)

    shell:"""
    ## step: Trimming with fastp for adapter and quality at Q15, removing adapters, and doing overlap correction
    if [[ "{params.ad}" == "None" ]]; then
      adapter_args=""
    else
      adapter_args="--adapter_sequence={params.ad} --adapter_sequence_r2={params.ad}"
    fi


    fastp -i {input.r2} -I {input.r2} \
                           -o {output.r1} -O {output.r2} \
                           -h {output.html} -j {output.json} \
                           ${{adapter_args}} \
                           -y -c --trim_poly_x \
                           -5 --cut_front_mean_quality 15 \
                           -3 --cut_tail_mean_quality 15 \
                           --unqualified_percent_limit 100 \
                           -e 15 \
                           -l 60 \
                           -w {threads}
     """

rule spades:
    threads: clust_conf["spades"]["threads"]
    envmodules: clust_conf["spades"]["modules"]
    resources: cc = repr(clust_conf["spades"])
    input: r1 = ancient(rules.trimwgs.output.r1),
           r2 = ancient(rules.trimwgs.output.r2),
           fake = ancient(rules.createsampledir.output)
    output: scaffolds = ASSEMBLYFASTA,
            contigs = protected(pjoin(SUBOUT, "spades", "contigs.fasta"))
    params: tempdir = pjoin(TMP, "spades"),
            outdir = pjoin(SUBOUT, "spades")
    shell:"""
    ## step: Running metaspades on trimmed reads
    rm -rf {params.outdir}

    trap 'mv -v {params.tempdir} {params.outdir}' EXIT
    metaspades.py -o {params.tempdir} -1 {input.r1} -2 {input.r2} --phred-offset 33 -t {threads}
    success=$?
    ## remove corrected reads files if successful
    if [ ${{success}} -eq 0 ]; then
	rm -rf {params.tempdir}/K*
        rm -rf {params.tempdir}/corrected
    fi

    """

rule map2assem:
    threads: clust_conf["map2assem"]["threads"]
    envmodules: *clust_conf["map2assem"]["modules"]
    resources: cc = repr(clust_conf["map2assem"]),
               h_vmem = clust_conf["map2assem"]["h_vmem"]

    input: scaffolds = rules.spades.output.scaffolds,
           r1 = rules.trimwgs.output.r1,
           r2 = rules.trimwgs.output.r2
    output: bam = BAM,
            metrics = pjoin(SUBOUT, "{sample}.bowtie2.markdup.metrics")
    params: index = pjoin(TMP, "bowtie2index", "{sample}"),
            indexdir = pjoin(TMP, "bowtie2index"),
            sam = pjoin(TMP, "{sample}.bowtie2.sam"),
            sortedbam = pjoin(TMP, "{sample}.sorted.bam"),
            markdupbam = pjoin(TMP, "{sample}.bowtie2.markdup.bam"),
            sampledir = SUBOUT

    shell:"""
    # step: Building bowtie2 index from spades assembled contigs and aligning trimmed reads. sort by read name with
    # samtools and markduplicates with picard.

    mkdir -p {params.indexdir}

    bowtie2-build --verbose --threads {threads} {input.scaffolds} {params.index}

    trap 'mv -v $TMPDIR/*am {params.sampledir}' EXIT

    # Aligning trimmed reads to bowtie2 index and output aligned reads to sam file
    bowtie2 --sensitive-local --no-unal -S {params.sam} -p {threads} -t -x {params.index} -1 {input.r1} -2 {input.r2}
    ls -sh {params.sam}
    samtools sort -l 0 -m {resources.h_vmem} -n -@ {threads} -O bam -o {params.sortedbam} {params.sam}
    rm {params.sam}

    total_mem={resources.h_vmem}
    mag="${{total_mem: -1}}"
    total_mem=$(echo "${{total_mem%?}} * {threads}" | bc)${{mag}}
    java -XX:ParallelGCThreads={threads} -Xmx${{total_mem}} -jar {clust_conf[map2assem][picard]} MarkDuplicates \
      I={params.sortedbam} \
      O={params.markdupbam} \
      M={output.metrics} \
      ASSUME_SORT_ORDER=queryname \
      VALIDATION_STRINGENCY=LENIENT

    rm {params.sortedbam}

    """
