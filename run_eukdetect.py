#!/usr/bin/env python3

import json
import os
import shutil
import sys
import yaml
import snakemake as sm

from os.path import join as pjoin


## expand bash env vars that may be in params
params = sm.io.Params(toclone=snakemake.params, custom_map = os.path.expandvars)
## input is reserved word in python
inputs = sm.io.InputFiles(toclone=snakemake.input, custom_map = os.path.expandvars)
outfile = os.path.basename(snakemake.output.filthits)

## remove previous results
shutil.rmtree(params.outdir, ignore_errors=True)
## make work output directory
os.makedirs(pjoin(params.tempdir, "aln"), exist_ok=True)
exitvalue = 0
try:
        with open(inputs.fastp_json, 'r') as fastpfile:
                fastp = json.load(fastpfile)
        readlen = fastp["summary"]["after_filtering"]["read1_mean_length"]
        ## https://github.com/allind/EukDetect/blob/master/rules/eukdetect.rules#L47
        readlen = max(int(round(readlen * 0.8, 0)), 60)
        sm.shell("""
        set -ex
        bowtie2 -p {snakemake.threads} --quiet --omit-sec-seq --no-discordant --no-unal \
                -x {params.db} \
	        -1 {inputs.r1} \
	        -2 {inputs.r2} >{params.tempdir}/out.sam

        perl -lane '$l =0; $F[5] =~ s/(\d+)[MX=DN]/$l+=$1/eg; print if $l > {readlen} or /^@/' {params.tempdir}/out.sam \
                    | samtools view -u -q 30 \
                    | samtools sort -o {params.tempdir}/aln/{params.s}_aln_q30_lenfilter.sorted.bam
                """)
        os.remove(pjoin(params.tempdir, "out.sam"))

        ## make config file
        print("making config file", params.config, file=sys.stderr)
        fwd_suffix = os.path.basename(inputs.r1).replace(params.s, "", 1)
        rev_suffix = os.path.basename(inputs.r2).replace(params.s, "", 1)
        edict = { 'eukdetect_dir': params.eukdetect_dir, 'database_dir': os.path.dirname(params.db), 'database_prefix': os.path.basename(params.db),
                  'paired_end': 'true', 'fwd_suffix': fwd_suffix, 'rev_suffix': rev_suffix, 'readlen': readlen, 'fq_dir': os.path.dirname(inputs.r1),
                  'output_dir': params.tempdir, 'samples': { params.s : None }}
        ## https://stackoverflow.com/questions/37200150/can-i-dump-blank-instead-of-null-in-yaml-pyyaml
        yaml.add_representer(type(None), lambda dumper, value: dumper.represent_scalar(u'tag:yaml.org,2002:null', ''))
        with open(params.config, 'w') as f:
                print(yaml.dump(edict), file = f)

        sm.shell("""
        set -ex
            snakemake --snakefile {params.eukdetect_dir}/rules/eukdetect.rules --configfile {params.config} --cores {snakemake.threads} -p filter -d {params.tempdir}
        """)
except Exception as e:
        ## catch any errors and print, set exit value to 1, so parent snakemake process registers the error.
        tb = sys.exc_info()
        fname = tb[2].tb_frame.f_code.co_filename
        print("ERROR:", tb[0].__name__, "from file", fname,
              "line", tb[2].tb_lineno, file=sys.stderr, end=":\n")
        print("    ", e, file=sys.stderr)
        exitvalue = 1
finally:
        ## move temp workdir to final directory
        shutil.move(params.tempdir, params.outdir)

sys.exit(exitvalue)
