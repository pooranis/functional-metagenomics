import sys
import re
import os
from os.path import join as pjoin
import snakemake.utils as sm
import yaml



## cluster config
def default_clust(cc):
    """adds __default__ values from cluster config to other
       rules lacking those keys"""
    dkeys = set(cc["__default__"].keys())
    rules = [k for k in cc if k != "__default__"]
    for r in rules:
        toadd = dkeys.difference(cc[r].keys())
        if toadd:
            cc[r].update(dict((k, cc["__default__"][k]) for k in toadd))
    return(cc)

with open(config["clusterfile"]) as f:
    clust_conf = default_clust(yaml.safe_load(f))



## inputs
R1 = lambda wildcards: pjoin(config["inputdir"], config["samples"][wildcards.sample] + config["r1"])
R2 = lambda wildcards: pjoin(config["inputdir"], config["samples"][wildcards.sample] + config["r2"])
SAMPLES = list(config["samples"].keys())

if SAMPLES is None:
    sys.exit("Sample prefixes are needed")


## output directory
workdir: config["workdir"]
OUT = config["outputdir"]
TRIM = config["trimdir"]
## output subdirectory for individual samples
SUBOUT = pjoin(OUT, "{sample}")

## temp directory
TMP = clust_conf["__default__"]["tmpdir"]
shell.prefix("export TMPDIR={TMP}; set -ve;")

################ TARGET OUTPUTS #########################
TRIMFILES =  expand(pjoin(TRIM, "{sample}_R{read}_ted.fastq.gz"), sample=SAMPLES, read=[1,2])
SPADESFASTA = expand(pjoin(SUBOUT, "spades", "scaffolds.fasta"), sample=SAMPLES)

# kraken outputs
config["kraken_conf"] = str(config["kraken_conf"])
kraken_dir = pjoin("kraken", os.path.basename(config["kraken_db"]))
KRAKENREPORT = expand(pjoin(SUBOUT, kraken_dir, "{sample}_scaffolds.kraken.report."+config["kraken_conf"]+".txt"), sample=SAMPLES)

BAMFILES = expand(pjoin(SUBOUT, "{sample}.bowtie2.markdup.bam"), sample=SAMPLES)

# prodigal outputs
prodigal_prefix = "{sample}.prd"
PRODIGAL = expand(pjoin(SUBOUT, "prodigal", prodigal_prefix+".{ext}"), sample=SAMPLES, ext=['fna', 'gff', 'faa', 'sco'])

# verse and humann
READCOUNTS = expand(pjoin(SUBOUT, "humann", "{sample}.count.CDS.txt"), sample=SAMPLES)
HUMANNPATH = expand(pjoin(SUBOUT, "humann", "{sample}.humann.mod_pathabundance.tsv"), sample=SAMPLES)

# eggnog-mapper
LOGS = pjoin(OUT, "logs")
EGGNOG = pjoin(LOGS, "eggnog_done.txt")


################ RULES ###########################
onerror:
     print("An error occurred in the functional metagenomics pipeline.")
     shell("mail -s 'Error in functional metagenomics pipeline.' {config[email]} < {log}")

localrules: all, createsampledir

rule all:
    input: ancient(TRIMFILES),SPADESFASTA,KRAKENREPORT,BAMFILES,PRODIGAL,READCOUNTS,HUMANNPATH,EGGNOG
    message: "Run all steps."


rule createsampledir:
    input: r1 = R1,
           r2 = R2
    output: temp(pjoin(SUBOUT, "snakefake"))
    params: outdir = SUBOUT,
            logdir = LOGS
    shell:"""
    mkdir -p {params.logdir}
    mkdir -p {params.outdir}
    touch {output}
    """


rule trimwgs:
    threads: clust_conf["trimwgs"]["threads"]
    log: sm.simplify_path(pjoin(OUT, "trimmed", "trimwgs.{sample}.jobid.*"))
    envmodules: clust_conf["trimwgs"]["modules"]
    input: r1 = ancient(R1),
           r2 = ancient(R2),
           fake = ancient(rules.createsampledir.output)
    output: r1 = pjoin(TRIM, "{sample}_R1_ted.fastq.gz"),
            r2 = pjoin(TRIM, "{sample}_R2_ted.fastq.gz"),
            json = pjoin(TRIM, "{sample}_fastp.json"),
            html = pjoin(TRIM, "{sample}_fastp_log.html")
    params: ad = config["adapter"]

    shell:"""
    ## step: Trimming with fastp for adapter and quality at Q15, removing adapters, and doing overlap correction

    fastp -i {input.r2} -I {input.r2} \
                           -o {output.r1} -O {output.r2} \
                           -h {output.html} -j {output.json} \
                           --adapter_sequence={params.ad} --adapter_sequence_r2={params.ad} \
                           -y -c --trim_poly_x \
                           -5 --cut_front_mean_quality 15 \
                           -3 --cut_tail_mean_quality 15 \
                           --unqualified_percent_limit 100 \
                           -e 15 \
                           -l 60 \
                           -w {threads}
     """

rule spades:
    threads: clust_conf["spades"]["threads"]
    envmodules: clust_conf["spades"]["modules"]
    resources: cc = repr(clust_conf["spades"])
    input: r1 = ancient(rules.trimwgs.output.r1),
           r2 = ancient(rules.trimwgs.output.r2)
    output: scaffolds = pjoin(SUBOUT, "spades", "scaffolds.fasta"),
            contigs = protected(pjoin(SUBOUT, "spades", "contigs.fasta"))
    params: tempdir = pjoin(TMP, "spades"),
            outdir = pjoin(SUBOUT, "spades")
    shell:"""
    ## step: Running metaspades on trimmed reads
    rm -rf {params.outdir}

    trap 'mv -v {params.tempdir} {params.outdir}' EXIT
    metaspades.py -o {params.tempdir} -1 {input.r1} -2 {input.r2} --phred-offset 33 -t {threads}
    success=$?
    ## remove corrected reads files if successful
    if [ ${{success}} -eq 0 ]; then
	rm -rf {params.tempdir}/K*
        rm -rf {params.tempdir}/corrected
    fi

    """


rule kraken:
    threads: clust_conf["kraken"]["threads"]
    envmodules: clust_conf["kraken"]["modules"]
    resources: cc = repr(clust_conf["kraken"])
    input: SPADESFASTA,
           db = config["kraken_db"]
    output: kreport = KRAKENREPORT,
            kout = expand(pjoin(OUT, "{sample}", kraken_dir, "{sample}_scaffolds.kraken."+config["kraken_conf"]+".out"), sample=SAMPLES)
    params: tempdir = pjoin(TMP, "kraken"),
            outdir = OUT,
            shmdir = clust_conf["kraken"]["shmdir"],
            parjobs = 4,
            samples = SAMPLES,
            kraken_dir = kraken_dir
    shell:"""
    ### run kraken
    DBNAME=$(basename {input.db})
    export DBNAME={params.shmdir}/${{DBNAME}}

    echo 'copying kraken database to /dev/shm'
    free -mh /dev/shm
    mkdir -p ${{DBNAME}}
    trap 'rm -rvf {params.shmdir}' EXIT ERR SIGTERM
    cp {input.db}/*.k2d ${{DBNAME}}/
    free -mh /dev/shm

    export PROCS=$(echo "{threads} / {params.parjobs}" | bc)
    echo $PROCS
    mkdir -p {params.tempdir}

    runkraken () {{
        kraken2 --threads $PROCS \
                    --memory-mapping \
                    --confidence {config[kraken_conf]} \
                    --db ${{DBNAME}} \
                    --output {params.tempdir}/${{1}}_scaffolds.kraken.{config[kraken_conf]}.out \
                    --report {params.tempdir}/${{1}}_scaffolds.kraken.report.{config[kraken_conf]}.txt \
                    {params.outdir}/${{1}}/spades/scaffolds.fasta

        sampledir={params.outdir}/${{1}}/{params.kraken_dir}
        mkdir -p ${{sampledir}}
        mv -v {params.tempdir}/${{1}}_scaffolds.kraken.* ${{sampledir}}/
    }}
    export -f runkraken

    parallel -j {params.parjobs} --tag runkraken ::: {params.samples}

    """

rule map2assem:
    threads: clust_conf["map2assem"]["threads"]
    envmodules: *clust_conf["map2assem"]["modules"]
    resources: cc = repr(clust_conf["map2assem"]),
               h_vmem = clust_conf["map2assem"]["h_vmem"]

    input: scaffolds = rules.spades.output.scaffolds,
           r1 = rules.trimwgs.output.r1,
           r2 = rules.trimwgs.output.r2
    output: bam = pjoin(SUBOUT, "{sample}.bowtie2.markdup.bam"),
            metrics = pjoin(SUBOUT, "{sample}.bowtie2.markdup.metrics")
    params: index = pjoin(TMP, "bowtie2index", "{sample}"),
            indexdir = pjoin(TMP, "bowtie2index"),
            sam = pjoin(TMP, "{sample}.bowtie2.sam"),
            sortedbam = pjoin(TMP, "{sample}.sorted.bam"),
            markdupbam = pjoin(TMP, "{sample}.bowtie2.markdup.bam"),
            sampledir = SUBOUT

    shell:"""
    # step: Building bowtie2 index from spades assembled contigs and aligning trimmed reads. sort by read name with
    # samtools and markduplicates with picard.

    mkdir -p {params.indexdir}

    bowtie2-build --verbose --threads {threads} {input.scaffolds} {params.index}

    trap 'mv -v $TMPDIR/*am {params.sampledir}' EXIT

    # Aligning trimmed reads to bowtie2 index and output aligned reads to sam file
    bowtie2 --sensitive-local --no-unal -S {params.sam} -p {threads} -t -x {params.index} -1 {input.r1} -2 {input.r2}
    ls -sh {params.sam}
    samtools sort -l 0 -m {resources.h_vmem} -n -@ {threads} -O bam -o {params.sortedbam} {params.sam}
    rm {params.sam}

    total_mem={resources.h_vmem}
    mag="${{total_mem: -1}}"
    total_mem=$(echo "${{total_mem%?}} * {threads}" | bc)${{mag}}
    java -XX:ParallelGCThreads={threads} -Xmx${{total_mem}} -jar {clust_conf[map2assem][picard]} MarkDuplicates \
      I={params.sortedbam} \
      O={params.markdupbam} \
      M={output.metrics} \
      ASSUME_SORT_ORDER=queryname \
      VALIDATION_STRINGENCY=LENIENT

    rm {params.sortedbam}

    """

rule genepred:
    threads: clust_conf["genepred"]["threads"]
    envmodules: *clust_conf["genepred"]["modules"]
    resources: cc = repr(clust_conf["genepred"])

    input: scaffolds = rules.spades.output.scaffolds
    output: gff = pjoin(SUBOUT, "prodigal", prodigal_prefix+".gff"),
            faa = pjoin(SUBOUT, "prodigal", prodigal_prefix+".faa"),
            fna = pjoin(SUBOUT, "prodigal", prodigal_prefix+".fna"),
            sco = pjoin(SUBOUT, "prodigal", prodigal_prefix+".sco")
    params: work = pjoin(TMP, "prodigal"),
            final = pjoin(SUBOUT, "prodigal"),
            prefix = prodigal_prefix,
            slist = pjoin(TMP, "list.txt"),
            s = "{sample}"
    shell:"""
    ## cleanup failed jobs
    rm -rf {params.final}

    ## on error mv temp to final outputdir
    trap 'mv -v $TMPDIR {params.final}' ERR

    scafpath=$(realpath {input.scaffolds})

    ## seqkit split makes files input.part_001.fasta
    seqkit split2 -p {threads} ${{scafpath}} -O $TMPDIR/
    skbase=$(ls $TMPDIR/*.part_[0-9][0-9][0-9]* | head -n1)
    skext=${{skbase##*.part_[0-9][0-9][0-9].}}
    skbase=`expr "$skbase" : '\(.*.part\)'`

    ## get just the part numbers in list to iterate over
    ls ${{skbase}}_[0-9][0-9][0-9].${{skext}} | perl -nE '$_ =~ /.+\.part_(\d+).+/; say $1' >{params.slist}
    ls $TMPDIR/

    cat {params.slist} | parallel -j {threads} prodigal -i ${{skbase}}_{{}}.${{skext}} \
                          -d $TMPDIR/{{}}.fna -a $TMPDIR/{{}}.faa \
                          -f gff -o $TMPDIR/{{}}.gff \
                          -s $TMPDIR/{{}}.sco \
                          -p meta

    mkdir -p {params.work}
    ## add sample ID to keep track when collating and part number to make sure IDs are distinct across parallel jobs to CDS ID
    cat {params.slist} | parallel -j1 -q sed "s/\tID=/\tID={params.s}{{}}_/" $TMPDIR/{{}}.gff >{params.work}/{params.prefix}.gff
    ## transform headers to only contain ID because humann and other tools don't like space in header; other info is in gff anyway
    cat {params.slist} | parallel -j1 -q seqkit replace -p '^.*?ID=(.+?);.+$' -r '{params.s}{{}}_$1' $TMPDIR/{{}}.fna >{params.work}/{params.prefix}.fna
    cat {params.slist} | parallel -j1 -q seqkit replace -p '^.*?ID=(.+?);.+$' -r '{params.s}{{}}_$1' $TMPDIR/{{}}.faa >{params.work}/{params.prefix}.faa
    cat {params.slist} | parallel -j1 cat $TMPDIR/{{}}.sco >{params.work}/{params.prefix}.sco

    mv -v {params.work} {params.final}
    """

rule humann:
    """run verse to get read counts and humann3 to get pathways"""
    threads: clust_conf["humann"]["threads"]
    envmodules: *clust_conf["humann"]["modules"]
    resources: cc = repr(clust_conf["humann"])

    input: bam = rules.map2assem.output.bam,
           gff = rules.genepred.output.gff,
           fna = rules.genepred.output.fna
    output: readcounts = pjoin(SUBOUT, "humann", "{sample}.count.CDS.txt"),
            genefam = pjoin(SUBOUT, "humann", "{sample}.humann.mod_genefamilies.tsv"),
            pathabund = pjoin(SUBOUT, "humann", "{sample}.humann.mod_pathabundance.tsv")
    params: work = pjoin(TMP, "humann"),
            temprc = pjoin(TMP, "humann", "{sample}.count"),
            final = pjoin(SUBOUT, "humann"),
            prefix = "{sample}.humann",
            modprefix =  "{sample}.humann.mod"
    shell:"""
    ## cleanup failed jobs
    rm -rf {params.final}
    mkdir -p {params.work}

    ## on exit mv temp to final outputdir
    trap 'mv -v {params.work} {params.final}' EXIT ERR SIGTERM

    verse -a {input.gff} -o {params.temprc} -z 1 -t CDS -g ID -l -T {threads} --ignoreDup {input.bam}

    humann --version
    humann -i {input.fna} -o {params.work} --output-basename {params.prefix} --threads {threads} --input-format fasta \
        --log-level INFO --metaphlan-options "-t rel_ab --bowtie2db {config[metaphlan_db]}"
    rm -fv {params.work}/*/*.sam
    rm -fv {params.work}/*/*.bt2
    rm -fv {params.work}/*/*.ffn

    {config[scriptdir]}/modify_humann_gene_counts.py -i {params.work} -p {params.prefix} -o {params.work}/{params.modprefix} -r {params.temprc}.CDS.txt
    if [[ -f "{params.work}/{params.modprefix}_genefamilies.tsv" ]]; then
        humann -i {params.work}/{params.modprefix}_genefamilies.tsv -o {params.work} --output-basename {params.modprefix} \
            --threads {threads} --input-format genetable --log-level INFO
    fi

    """

#### check output of failed rule for aggregation example starts here #######
rule em_align:
    """run eggnog-mapper on translated protein sequences from prodigal"""
    threads: clust_conf["em_align"]["threads"]
    envmodules: clust_conf["em_align"]["modules"]
    resources: cc = repr(clust_conf["em_align"])

    input: faa = rules.genepred.output.faa,
           gff = rules.genepred.output.gff
    output: check = pjoin(SUBOUT, "eggnog", "{sample}.check.txt")
    params: work = pjoin(TMP, "eggnog"),
            final = pjoin(SUBOUT, "eggnog"),
            s = "{sample}",
            tmp = pjoin(TMP, "tmp")
    shell:"""
    set +e
    mkdir -p {params.final}
    rm -rf {output.check}
    touch {output.check}
    mkdir -p {params.work}
    mkdir -p {params.tmp}


    ## on exit mv work files to final outputdir
    trap 'mv -vf {params.work}/* {params.final}/' EXIT


    emapper.py -i {input.faa} -o {params.s} --output_dir {params.work} --cpu {threads} --itype proteins \
        --block_size 3 --index_chunks 2 --override --temp_dir {params.tmp} --no_annot

    if [[ $? -eq 0 ]]; then
        echo "success" >{output.check}
    fi

    """


# input function for the rule em_annot
def aggregate_eggnog(wildcards, input):
    """https://github.com/snakemake/snakemake/issues/16"""
    success_samples = set()
    print("Failed em_align samples:", file=sys.stderr)
    for i in input:
        sample = os.path.basename(i).removesuffix('.check.txt')
        if os.path.getsize(i)>0:
            success_samples.add(sample)
        else:
            print(sample, file=sys.stderr)
    success_samples = list(success_samples)
    if len(success_samples) == 0:
        raise RuntimeError('No samples successfully completed eggnog alignment.')
    return(success_samples)




rule em_annot:
    """run eggnog-mapper annotation on eggnog orthologs output of alignment"""
    threads: clust_conf["em_annot"]["threads"]
    envmodules: clust_conf["em_annot"]["modules"]
    resources: cc = repr(clust_conf["em_annot"])

    input: expand(rules.em_align.output.check, sample=SAMPLES)
    output: EGGNOG
    params: work = pjoin(TMP, "eggnog"),
            indir = OUT,
            shmdir = clust_conf["em_annot"]["shmdir"],
            parjobs = clust_conf["em_annot"]["parjobs"],
            gff = prodigal_prefix.replace('{sample}', '')+'.gff',
            failed = pjoin(LOGS, "failed.em_annot.log"),
            samples = aggregate_eggnog

    shell:"""
    date >>{params.failed}
    ## run eggnog-mapper annotation on eggnog orthologs output of alignment
    ## copy database to /dev/shm
    mkdir -p {params.shmdir}
    trap 'rm -rvf {params.shmdir}' EXIT ERR SIGTERM
    echo 'copying eggnog database to /dev/shm'
    free -h --wide
    cp $EGGNOG_DATA_DIR/eggnog.db {params.shmdir}/
    cp $EGGNOG_DATA_DIR/eggnog_proteins.dmnd {params.shmdir}/
    free -h --wide

    export PROCS=$(echo "{threads} / {params.parjobs}" | bc)
    echo $PROCS
    mkdir -p {params.work}

    runeggnog () {{
      indir={params.indir}/${{1}}
      emapper.py --annotate_hits_table ${{indir}}/eggnog/${{1}}.emapper.seed_orthologs -o ${{1}} \
              --output_dir {params.work} --cpu $PROCS \
              --override --temp_dir $TMPDIR --data_dir {params.shmdir} \
              --decorate_gff ${{indir}}/prodigal/${{1}}{params.gff} --decorate_gff_ID_field ID \
              2>&1 | tee ${{indir}}/em_annot.${{1}}.log

      exitcode=$?
      if [[ ${{exitcode}} -ne 0 ]]; then
         echo ${{1}} >>{params.failed}
      fi

      mv -vf {params.work}/${{1}}.emapper.* ${{indir}}/eggnog/

    }}
    export -f runeggnog

    parallel -j {params.parjobs} --tag runeggnog ::: {params.samples}
    touch {output}
    ### failed samples will be in {params.failed}
    """
