#!/usr/bin/env python3
"""
See on Biowulf: /data/BCBB_microbiome_db/metacyc/README.md

May have to convert *.dat files to remove special characters.
This seems to work:
iconv -c -f ISO-8859-16 -t UTF-8 ../data/pathways.dat >pathways.dat

"""

import re
import sys
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('pwyfile', metavar='PATHWAY_FILE', type=str,
                    help='pathways.dat file')
parser.add_argument('rxnfile', metavar='REACTIONS_FILE', type=str,
                    help='reactions.dat file')
parser.add_argument('-m', '--mapfile', type=str,
                    help='output file mapping from pathway to EC number (default: %(default)s)',
                    default='metacyc.pwy2ec.txt')
parser.add_argument('-p', '--pwyhierarchy', type=str,
                    help='output file mapping from pathway to pathway name (default: %(default)s)', default='metacyc.pwy2hierarchy.txt')
args = parser.parse_args()
print(args, file=sys.stderr)


def getvalue(l):
    l = l.strip()
    value = re.sub('^[A-Z-]+\s\-\s*', '', l)
    return(value)


with open(args.pwyfile, 'r', errors='surrogateescape') as f:
    id = str()
    name = {}
    type = {}
    classes = set()
    pwys = set()
    for line in f:
        if line.startswith('UNIQUE-ID'):
            id = getvalue(line)
            pwys.add(id)
        elif line.startswith('//'):
            id = str()
        elif line.startswith('TYPES') and len(id) > 0:
            supertype = getvalue(line)
            if supertype == "Super-Pathways":
                continue
            try: type[id].append(supertype)
            except KeyError: type[id] = [supertype]
            classes.add(supertype)
        elif line.startswith('COMMON-NAME') and len(id) > 0:
            name[id] = getvalue(line)


with open(args.pwyhierarchy, 'w') as f:
    for id in name.keys():
        print(f"{id}\t{name[id]}", file=f)

with open(args.rxnfile, 'r', errors='surrogateescape') as f:
    ec = set()
    inpwy = {}
    for line in f:
        if line.startswith('EC-NUMBER'):
            myec = getvalue(line).replace('EC-', '', 1)
            ec.add(myec)
        elif line.startswith('//'):
            ec = set()
        elif line.startswith('IN-PATHWAY') and len(ec) > 0:
            mypwy = getvalue(line)
            try: inpwy[mypwy].update(ec)
            except KeyError: inpwy[mypwy] = ec

with open(args.mapfile, 'w') as f:
    for pwy in inpwy.keys():
        for ec in inpwy[pwy]:
            print(f"{pwy}\t{ec}", file=f)

# with open(classfile, 'r') as f:
#     id = str()
#     name = {}
#     type = {}
#     for line in f:
#         if line.startswith('UNIQUE-ID'):
#             id = getvalue(line)
#         elif line.startswith('//'):
#             id = str()
#         elif line.startswith('TYPES') and len(id) > 0:
#             supertype = getvalue(line)
#             if supertype == "Pathways":
#                 continue
#             try: type[id].append(supertype)
#             except KeyError: type[id] = [supertype]
#         elif line.startswith('COMMON-NAME') and len(id) > 0:
#             name[id] = getvalue(line)


# ## hierarchy mapping
# hmap = {}

# for c in classes:
#     try:
#         parents = type[c]
#     except:
#         hmap[c] = [c]
#         continue
#     if len(parents) > 1:
#         print(f"{c} has more than one type", file=sys.stderr)
#         print(parents, file=sys.stderr)
#         hmap[c] = [c]
#         continue
