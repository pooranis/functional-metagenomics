Helpful Bits for KEGG Annotation
=================================

.. contents:: :backlinks: top

.. sectnum:: :suffix: .

ghostkoalapathwayparse.py
----------------------------------------------------------

.. raw:: html

  <pre><a href="ghostkoalapathwayparse.py"><b>ghostkoalapathwayparse.py</b></a> [-h] [-k FILENAME] [-p PATH] koala</pre>

This script takes in a text file which is made by processing Ghost Koala results kegg mapper html file with pandoc::

   pandoc -i koala-kegg-mapper.html --wrap none -o koala.txt

and outputs a tab-delimited table of pathway hierarchy and count to stdout.

positional arguments:
  /koala                 text file

optional arguments:
  -h, --help            show this help message and exit
  -k FILENAME, --ko FILENAME
                        Also output a table of the pathway ids and their KO numbers to FILENAME.
  -p PATH, --pandoc PATH
                        This script can run pandoc for you, if you specify the path of the pandoc
                        executable to use. Then <koala> can just be the html file, and it will be
                        converted to file <koala>.txt.

parsekegg.pl
-------------------------------

.. raw:: html

  <pre><a href="parsekegg.pl"><b>parsekegg.pl</b></a> [options] inputfile [pathwaymap]</pre>

This script parses the kegg orthology/hierarchy downloaded from kegg.jp
webpage: https://www.genome.jp/kegg-bin/get_htext?ko00001.keg, and formats it into a
tab-delimited table.
It can also output just the pathway hierarchy table for use with `genes.to.kronaTable.py <../genes.to.kronaTable.py>`_.
It can also format the kegg ec to pathway map into a map file for use with MinPath.

/inputfile   (required) KEGG htext file from https://www.genome.jp/kegg-bin/download_htext?htext=ko00001.keg&format=htext
/pathwaymap  (optional) KEGG pathway map file from http://rest.kegg.jp/link/pathway/ec

Options:

-o <outputfile>     Output kegg orthology table file [default: STDOUT]
-y <hierarchyfile>  Also produce pathway hierarchy table file.  If -y passed with no argument,
                    default table filename: kegg.ec.hierarchy
-p <ec2pathfile>    If pathway map is given as second arg, this is output ec2path filename
                    [default: minpath.kegg.ec_pathway.map]
-v                  Verbose.  Requires Perl module Data::Dumper::Names to be installed.
