#!/usr/bin/env python3

import sys
import re
import argparse


# Help and Argument Parsing
parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description='''This script takes in a text file which is made by processing Ghost Koala results kegg mapper html file with pandoc:

    pandoc -i koala-kegg-mapper.html --wrap none -o koala.txt

and outputs a tab-delimited table of pathway hierarchy and count to stdout.
''')
parser.add_argument('koala', help='text file')
parser.add_argument('-k', '--ko', metavar="FILENAME", type=argparse.FileType('w'),
                    help="Also output a table of the pathway ids and their KO numbers to %(metavar)s.")
parser.add_argument('-p', '--pandoc', metavar="PATH",
                    help="This script can run pandoc for you, if you specify the path of the pandoc executable to use.  Then <koala> can just be the html file, and it will be converted to file <koala>.txt.")
args = parser.parse_args()

## run pandoc, if needed
if args.pandoc:
    import subprocess
    outfile = args.koala + '.txt'
    cmd = [args.pandoc, '-f', 'html', '-i', args.koala, '--wrap', 'none', '-o', outfile]
    print('Running', ' '.join(cmd), file=sys.stderr)
    res = subprocess.check_call(cmd)
    args.koala = outfile

C1 = str()  #: Top level category in pathway hierarchy
C2 = str()  #: Second level category
mapid = str()  #: pathway id
mapname = str()  #: pathway name

## Print header of output file
print('C1', 'C2', 'id', 'name', 'count', sep='\t')
if args.ko:
    print('C1', 'C2', 'id', 'name', 'KO', sep='\t', file=args.ko)


## Loop through Ghost Koala result file
with open(args.koala, 'r') as f:
    print("Looping through", args.koala, file=sys.stderr)
    for line in f:
        if line.startswith('**'):
            ## **Genetic Information Processing**
            C1 = line.strip().replace('**', '')
        elif re.match('[A-Za-z]+', line):
            ## Transcription
            C2 = line.strip()
        elif line.startswith('-'):
            ## -   [03020](https://www.kegg.jp/kegg-bin/show_pathway?161616833925389/map03020.coords+reference) RNA polymerase ([19](javascript:display('map03020')))
            m = re.match(
                r'-\s+\[\d+\]\(.+\)\s(.+)\s+\(\[(\d+)\]\(javascript\:display\(\'(.+)\'\)\)\)', line)
            if m:
                print(C1, C2, m.groups()[2], m.groups()[0],
                      m.groups()[1], sep='\t')
                if args.ko:
                    mapid = m.groups()[2]
                    mapname = m.groups()[0]
        elif args.ko and line.startswith('    [K'):
            ## [K03002](https://www.kegg.jp/dbget-bin/www_bget?K03002)
            m = re.match(r'\s+\[(.+)\]', line)
            print(C1, C2, mapid, mapname, m.groups()
                  [0], sep='\t', file=args.ko)

## close KO table output file
if args.ko:
    args.ko.close()
