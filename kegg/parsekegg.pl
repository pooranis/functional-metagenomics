#!/usr/bin/env perl

## Download KEGG htext file from https://www.genome.jp/kegg-bin/get_htext?ko00001.keg.  This is input for this script

use 5.010;
use strict;
use String::Util 'trim';
use Scalar::Util 'refaddr';

use Getopt::Long qw(:config no_auto_abbrev);

my $OUTFILE;
my $hierarchy;
my $hout = "kegg.ec.hierarchy";
my $ec2path = "minpath.kegg.ec_pathway.map";
my $verbose = 0;


### Arg parsing start
GetOptions("h|help" => sub { HELP_MESSAGE(0) },
	   "o|output=s" => sub { open($OUTFILE, ">$_[1]") },
	   "y|hierarchy:s" => sub { if ($_[1]) { $hierarchy = $_[1] } else { $hierarchy = $hout } },
	   "p|pathmap=s" => \$ec2path,
	   "v|verbose+" => \$verbose);

my ($infile, $inpathfile) = @ARGV;

if (!$infile) {
  say STDERR "ERROR: no input file given.";
  HELP_MESSAGE(1);
}

sub HELP_MESSAGE {
  my ($status) = @_;
  say "
Usage parsekegg.pl [options] inputfile [pathwaymap]

inputfile   (required) KEGG htext file from https://www.genome.jp/kegg-bin/download_htext?htext=ko00001.keg&format=htext
pathwaymap  (optional) KEGG pathway map file from http://rest.kegg.jp/link/pathway/ec

Options:
-o <outputfile>    Output kegg orthology table file [default: STDOUT]
-y [hierarchyfile] Also produce pathway hierarchy table file.  If -y passed with no argument,
                   default table filename: $hout
-p <ec2pathfile>   If pathway map is given as second arg, this is output ec2path filename
                   [default: $ec2path]
-v                 Verbose.  Requires Perl module Data::Dumper::Names to be installed.

This script parses the kegg orthology/hierarchy downloaded from kegg.jp
webpage: https://www.genome.jp/kegg-bin/get_htext?ko00001\.keg, and formats it into a
tab-delimited table.
It can also output just the pathway hierarchy table for use with genes.to.KronaTable.py.
It can also format the kegg ec to pathway map into a map file for use with MinPath.
  ";
  exit($status);

}

#### Arg parsing done

#### Verbosity
if ($verbose) {
  require Data::Dumper::Names;
  Data::Dumper::Names->import( qw(Dumper) );
}
####


## open kegg orthology input file
open my $INFILE, $infile or die "can't open $infile";

## skip first 3 lines of input file
<$INFILE>;
<$INFILE>;
<$INFILE>;

my $A;
my $Anum;
my $B;
my $Bnum;
my $C;
my $Cnum;
my $Cpath;
my $Cbrite;
my %pathways;


# redirect stdout to file if defined
select $OUTFILE if ($OUTFILE) ;

## headers for output table
say join("\t", ("kegg", "name", "description", "Cnum", "C", "Cpath", "Cbrite", "Bnum", "B", "Anum", "A"));

## loop through input file
while (<$INFILE>) {
  if ($_ =~ /^A/) {
    $_ =~ /A(\d+)(.+$)/;
    $Anum = $1;
    $A = trim($2);
    warn Dumper($Anum, $A) if ($verbose);
  } elsif ($_ =~ /^B/ && $_ =~ /\d/) {
    $_ =~ /^B\s+(\d+)(.+$)/;
    $Bnum = $1;
    $B = trim($2);
    warn Dumper($Bnum, $B) if ($verbose > 1);
  } elsif ($_ =~ /^C/) {
    if ($_ =~ /\[PATH\:/) {
      $_ =~ /^C\s+(\d+)(.+)\[PATH\:(\w+)\]/;
      $Cnum = $1;
      $C = trim($2);
      $Cpath = $3;
      $Cbrite = "";
    } elsif ($_ =~ /\[BR\:/) {
      $_ =~ /^C\s+(\d+)(.+)\[BR\:(\w+)\]/;
      $Cnum = $1;
      $C = trim($2);
      $Cpath = "";
      $Cbrite = $3;
    } else {
      $_ =~ /^C\s+(\d+)(.+$)/;
      $Cnum = $1;
      $C = trim($2);
      $Cpath = "";
    }
#    warn Dumper($Cnum, $C, $Cpath, $Cbrite);
  } elsif ($_ =~ /^D/) {
    $_ =~ /^D\s+(K\d+)(.+?)\;(.+$)/;
    my $Dnum = $1;
    my $symbol = trim($2);
    my $name = trim($3);

    ## for each K number, print metadata and hierarchy row
    say join("\t", ($Dnum, $symbol, $name, $Cnum, $C, $Cpath, $Cbrite, $Bnum, $B, $Anum, $A));

    ## save just the pathway part to hash in case pathway table only is also needed
    $pathways{join("\t", ("map$Cnum", $C, "$B|$A"))}++;
  }
}

select STDOUT;
close $INFILE;
close $OUTFILE if ($OUTFILE);


## print just the pathway hierarchy table - not including the
if ($hierarchy) {
  say STDERR "writing hierarchy file $hierarchy";

  open my $HFILE, ">$hierarchy";

  say $HFILE join("\t", ("Cnum", "C", "levels"));

  foreach my $pwy (keys %pathways) {
    say $HFILE $pwy;
  }

  close $HFILE;

}


## print ec2path file for MinPath
if ($inpathfile) {
  say STDERR "converting kegg ec pathway map file $inpathfile to ec2path formatted file $ec2path for MinPath.";
  open my $PATHFILE, "$inpathfile" or die "can't open $inpathfile";
  open my $OUTP, ">$ec2path";

  say $OUTP "#KEGG pathway and ec mapping file";
  say $OUTP "#Pathway\tEC";

 LINE: while (<$PATHFILE>) {
    ## only get pathway mapping
    next LINE if $_ =~ /\tpath\:ec\d+/;
    my @F = split(' ', $_);
    $F[1] =~ s/^path\://;
    $F[0] =~ s/^ec\://;
    say $OUTP join("\t", ($F[1], $F[0]));
  }

  close $PATHFILE, $OUTP;
}
