#!/usr/bin/env python3

import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__)))
from GTF import featurelines

import pandas as pd
import argparse

parser = argparse.ArgumentParser(description='Map from kraken taxonomy assignment to predicted gene from humann')
req = parser.add_argument_group('required arguments')
req.add_argument('-p', '--pred2uniref_file', type=str,
                 help='map from predicted gene name (locus tag) to uniref gene. output of modify_humann_gene_counts.py', required=True)
req.add_argument('-k', '--kraken', type=str,
                    help='raw output of kraken', required=True)
req.add_argument('-g', '--gtf', type=str,
                    help='gtf or gff file which has contig and gene ID info (output of prodigal)', required=True)
parser.add_argument('-o', '--output', type=str, help='output prefix. (default: %(default)s)',
                    default="out")

parser._action_groups.reverse()
args = parser.parse_args()



outsfx = '.locus_tag2contig.txt'



def locustag2contig(filename, filterval, filtercol="ID", feature='CDS'):
    """Open an optionally gzipped GTF file, filter for lines containing ``feature``
       and return a dict
       mapping ``locus_tag`` to ``seqname`` contig.
    """
    # Each column is a list stored as a value in this dict.
    result = {}

    for line in featurelines(filename, feature=feature):
        if filtercol not in line.keys():
            continue
        elif line[filtercol] not in filterval:
            continue
        result[line[filtercol]] = line['seqname']

    return(result)


def mapgene2contig(infile, gtfile, outprefix, krakenfile):
    outfile = os.path.join(outprefix + outsfx)

    locus2uniref = pd.read_csv(infile, sep = '\t', usecols = ["pred_gene", "uniref", "class"]).drop_duplicates()
    locus = set(locus2uniref["pred_gene"])
    gtf = locustag2contig(gtfile, locus)
    del locus

    contigs = set(gtf.values())
    contig2tax = {}
    with open(krakenfile, 'r') as kf:
        for line in kf:
            field = line.split('\t')
            if field[1] in contigs:
                contig2tax[field[1]] = field[2]

    locus2taxid = pd.DataFrame.from_dict(gtf, orient = "index", columns = ["contig"])
    del gtf
    # mydf["taxid"] = contig2tax[mydf["contig"]]
    locus2taxid["taxid"] = locus2taxid['contig'].map(contig2tax)
    del contig2tax
    merged = pd.merge(locus2uniref, locus2taxid, how = "outer", left_on = "pred_gene", right_index = True, indicator = True)

    print("writing locus tag to uniref gene to contig to taxid map to", outfile, file=sys.stderr)
    merged.to_csv(outfile, sep = '\t', index = False)


### test on single sample and without multiproc
output = mapgene2contig(args.pred2uniref_file, args.gtf, args.output, args.kraken)
