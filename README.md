# Functional Metagenomics Pipeline

Loosely based on [this SciLife workflow](https://metagenomics-workshop.readthedocs.io/en/latest/annotation/index.html) - now  significantly diverged

Implemented with [snakemake](https://snakemake.readthedocs.io/en/stable/index.html) for running on Biowulf or Locus HPC at NIH (maybe could modify the cluster config files to run elsewhere).

### Requirements

Needs the following files:

- Recommended to keep these files in same *scriptdir* script director:
  - [functional_metagenomics.smk](functional_metagenomics.smk) - main snakefile 
  - [modify_humann_gene_counts.py](modify_humann_gene_counts.py) - see pipeline steps
  - [calc_tpm.py](calc_tpm.py) - see pipeline steps
  - clusterfile.yaml - cluster configuration - for submitting jobs to cluster mananagement system in submit_fm.sh , will vary according to environment.
    -  [fm.biowulf.yaml](fm.biowulf.yaml) or [fm.locus.yaml](fm.locus.yaml)
    - **Important parameters**  used in the snakefile (and therefore *required to be included* regardless of environment) for *ALL steps* are ``tmpdir``  and ``threads``.  Other steps have a few extra requirements, see these ^ configs for info.
    - The ``kraken`` and ``em_annot`` steps make use of large databases.  For efficiency, we copy those databases into */dev/shm*, so for those steps you should request twice the size of the db in total memory.
    - some modules are not globally installed (the cluster configs mention them) - still working on that.  For now, if you want to use them, in your submit script and your bashrc you can add their path to your MODULEPATH.  ``export MODULEPATH=$MODULEPATH:/path/to/new/modules``
- I keep these in my working/project directory because I modify them often according to the project:
  - See [fm.config.example.yaml](fm.config.example.yaml) for example project config file (usually keep in working directory)
    - for specifying directories for input/output, scripts, and also dbs, parameters and filenames for the pipeline 
  - [submit_fm.sh](submit_fm.sh) - example pipeline job submit script that you can keep in whatever directory you wish (I usually keep in working dir).  will need to edit for whichever cluster you are using - you'll see where to edit to put in your own project config file and paths, etc.  ***Need to use qsub/sbatch to submit submit_fm.sh.***

- Additional dependencies, see [Steps in Pipeline](#steps-in-pipeline)

### Notes

- This pipeline makes one directory per sample within the specified output directory
- The pipeline doesn't stop on error, and keeps going processing as many samples as it can to the end.  
  - It's not entirely fault-tolerant and requires some babysitting especially for the aggregate ``kraken`` step (not crucial, could skip by deleting KRAKENREPORT input to rule ``all`` in snakefile) and the ``em_annot`` steps which won't run if an earlier step fails for *any sample in the config*.  
  - ``em_annot`` takes a few hours to process all the samples all together and ``kraken`` takes even less time, so I usually run the whole pipeline, look into any errors and fix whatever, edit the config file to remove any failed samples that I am giving up on, and re-run the pipeline.  This will re-run any failed steps for samples in the config and the aggregate steps, if those failed steps now succeed.

### Steps in Pipeline

1. **trimwgs**: trim with [fastp](https://github.com/OpenGene/fastp)
2. **spades**: assembly with [metaspades](https://cab.spbu.ru/software/spades/)
3. **map2assem**: map trimmed reads to assembly with [bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml), mark duplicates with [picard](https://gatk.broadinstitute.org/hc/en-us/articles/360037052812-MarkDuplicates-Picard-)
4. **genepred**: predict genes with [prodigal](https://github.com/hyattpd/prodigal/wiki) and get read counts for predicted genes with [verse](https://github.com/kimpenn/VERSE).
5. **kraken**: taxonomic assignment of scaffolds with [kraken](https://github.com/DerrickWood/kraken2/wiki) (this pipeline doesn't do the real taxonomic assignment with reads - I do that separately because of memory requirements)
6. **humann**: annotate genes and infer pathways with [humann](https://github.com/biobakery/humann) using these read counts via [modify_humann_gene_counts.py](modify_humann_gene_counts.py)
7. **em_align**: use [eggnog-mapper](https://github.com/eggnogdb/eggnog-mapper/wiki) to align genes to database
8. **em_annot**: eggnog-mapper annotates aligned gene orthologs output by `em_align`
8. **card_rgi:** [RGI](https://github.com/arpcard/rgi) annotates genes from `genepred` step using [CARD database](https://card.mcmaster.ca/download).  use [calc_tpm.py](calc_tpm.py) to calculate tpm from output of `verse` in `humann` step and merge with RGI output.


## Other Files

- **[check_output_aggregate_example.smk](check_output_aggregate_example.smk)** example of how to aggregate when some samples fail for a rule.  see the em\_align and em\_annot rules and compare to original
