#!/bin/bash

module load signalp/4.1
module load chlorop/1.1
module load tmhmm/2.0c
module load targetp/1.1

module load FASTX-Toolkit/0.0.14-goolf-1.7.20
export PATH=/nethome/misnerij/software/WolfPSORT/bin:$PATH
#export PATH=/hpcdata/bcbb/poorani/mucor/secretome:$PATH


stdbuf -oL ./secretome_pipe.py ${1}.EVM.all.proteins.fasta "${1}." 2>&1 | tee ${1}.logfile.txt

