"""
rules for classifying taxonomy
- kraken for scaffolds
- eukdetect uses trimmed reads

1. a) To run kraken, config file MUST contain:

kraken_db: /path/to/kraken_db

   b) To run eukdetect, config file MUST contain:

eukdetect_db: /path/to/eukdetect_db

2.  If you used trim_assemble_map.smk to make the assemblies and bam files, then it will be included by default::

    include: setup.smk
    include: taxonomy.smk

2. To override defaults from trim_assemble_map.smk, specify in config file.  Example::

## pattern/format for assembly - should use {sample} wildcard.  only needed as input to kraken
ASSEMBLYFASTA: "outdir/{sample}_spades.fasta"

## TRIM: pattern/format for trimmed files using {sample} and {read} wildcards and any other vars in setup.smk. only needed for eukdetect
TRIM: "trimmedfiles/{sample}_R{read}_ted.fastq.gz"

## TRIMJSON: pattern for fastp json output needed to get read length for eukdetect
TRIMJSON: "trimmedfiles/{sample}_fastp.json"

"""
import os
from os.path import join as pjoin
import sys

#### INPUTS ##########

# check if trim_assemble_map.smk already included otherwise include it
if '__createtrimdir' not in vars():
    include: pjoin(config["scriptdir"], "trim_assemble_map.smk")
    print("including trim_assemble_map.smk from taxonomy.smk", file=sys.stderr)


## use config if specified instead
ASSEMBLYFASTA = config.get('ASSEMBLYFASTA', ASSEMBLYFASTA)
TRIM = config.get('TRIM', TRIM)
TRIMJSON = config.get('TRIMJSON', TRIMJSON)

## KRAKENINPUT the name of ALL assembly file names as a list
## (important because kraken rule runs on ALL assemblies at once - NOT sample-by-sample)
KRAKENINPUT = expand(ASSEMBLYFASTA, sample=SAMPLES)

################ TARGET OUTPUT VARS #########################

# kraken outputs
config["kraken_conf"] = str(config["kraken_conf"])
kraken_dir = pjoin("kraken", os.path.basename(config["kraken_db"]))
kraken_prefix = "{sample}_"+os.path.splitext(os.path.basename(ASSEMBLYFASTA))[0]+".kraken"
kraken_out = pjoin(SUBOUT, kraken_dir, kraken_prefix+'.'+config["kraken_conf"]+".out")
KRAKENREPORT = expand(pjoin(SUBOUT, kraken_dir, kraken_prefix+".report."+config["kraken_conf"]+".txt"), sample=SAMPLES)
sample2kraken = [ s + "," + ASSEMBLYFASTA.format(sample=s) + "," + kraken_prefix.format(sample=s) for s in SAMPLES ]

# eukdetect
EUKDETECT = pjoin(SUBOUT, "eukdetect", "{sample}_filtered_hits_taxonomy.txt")



################ RULES ###########################

rule kraken:
    threads: clust_conf["kraken"]["threads"]
    envmodules: clust_conf["kraken"]["modules"]
    resources: cc = repr(clust_conf["kraken"])
    input: KRAKENINPUT,
           db = config["kraken_db"],
           fake = ancient(rules.createlogdir.output)
    output: kreport = KRAKENREPORT,
            kout = expand(kraken_out, sample=SAMPLES)
    params: tempdir = pjoin(TMP, "kraken"),
            outdir = OUT,
            shmdir = clust_conf["kraken"]["shmdir"],
            parjobs = 4,
            samples = sample2kraken,
            kraken_dir = kraken_dir
    shell:"""
    ### run kraken
    DBNAME=$(basename {input.db})
    export DBNAME={params.shmdir}/${{DBNAME}}

    echo 'copying kraken database to /dev/shm'
    free -mh /dev/shm
    mkdir -p ${{DBNAME}}
    trap 'rm -rvf {params.shmdir}' EXIT ERR SIGTERM
    cp {input.db}/*.k2d ${{DBNAME}}/
    free -mh /dev/shm

    export PROCS=$(echo "{threads} / {params.parjobs}" | bc)
    echo $PROCS
    mkdir -p {params.tempdir}

    runkraken () {{
        input=${{2}}
        prefix=${{3}}
        kraken2 --threads $PROCS \
                    --memory-mapping \
                    --confidence {config[kraken_conf]} \
                    --db ${{DBNAME}} \
                    --output {params.tempdir}/${{prefix}}.{config[kraken_conf]}.out \
                    --report {params.tempdir}/${{prefix}}.report.{config[kraken_conf]}.txt \
                    ${{input}}

        sampledir={params.outdir}/${{1}}/{params.kraken_dir}
        mkdir -p ${{sampledir}}
        mv -v {params.tempdir}/${{prefix}}.* ${{sampledir}}/
    }}
    export -f runkraken

    parallel -j {params.parjobs} -C$"\," --tagstring {{1}} runkraken ::: {params.samples}

    """


rule eukdetect:
    """run Eukdetect https://github.com/allind/EukDetect"""
    threads: clust_conf["eukdetect"]["threads"]
    envmodules: clust_conf["eukdetect"]["modules"]
    resources: cc = repr(clust_conf["eukdetect"])

    input: fastp_json = TRIMJSON,
           r1 = TRIM.format(read=1, sample="{sample}"),
           r2 = TRIM.format(read=2, sample="{sample}")
    output: filthits = EUKDETECT
    params: db = config["eukdetect_db"],
            eukdetect_dir = clust_conf["eukdetect"]["dir"],
            tempdir = pjoin(TMP, "eukdetect"),
            outdir = pjoin(SUBOUT, "eukdetect"),
            config = pjoin(TMP, "eukdetect", "{sample}_eukdetect_config.yml"),
            s = "{sample}",
            systemtemp = TMP
    script: pjoin(config["scriptdir"], "run_eukdetect.py")
