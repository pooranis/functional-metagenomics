#!/usr/bin/env python3

##
## taxonomy conda environment
##

import argparse
import os
import re
import sys
from errno import EEXIST
from pprint import pprint

import pandas as pd
import taxopy

parser = argparse.ArgumentParser(description='Use external taxonomy assignment with humann genefamilies output')
req = parser.add_argument_group('required arguments')
req.add_argument('-l', '--locus2taxid_file', type=str,
                    help='mapping of locus tag to contig to taxid.  output of genes2contig2taxid.py', required=True)
req.add_argument('-i', '--input', type=str,
                    help='genefamilies file from humann', required=True)
parser.add_argument('-o', '--output', type=str, help='output prefix. (default: %(default)s)',
                    default="out")
parser.add_argument('--ncbitax', type=str,
                    help='location of ncbi taxonomy. If the taxonomy files don\'t exist, script will try to download the taxonomy there. (default: %(default)s)',
                    default="/fdb/taxonomy")
parser.add_argument('--nosp', action='store_true', help='ADVANCED.  Collapse sp. species.')

# parser.add_argument('-t', '--threads', type=int, help='number of threads. (default: %(default)s)',
#                     default=os.getenv("SLURM_CPUS_PER_TASK", default = 1))

parser._action_groups.reverse()
args = parser.parse_args()
pprint(vars(args), stream = sys.stderr, sort_dicts=False)

## check that new genefamilies filename is not the same as old/input
gf_outfile = args.output + "_genefamilies.tsv"
if gf_outfile == args.input:
    msg = "Input filename " + args.input + " is same as one of the output files of this script.  Please change the output prefix from '" + args.output + "' to something else to avoid overwriting."
    raise OSError(EEXIST, msg, args.input)

## load ncbi taxonomy
NCBITAX = args.ncbitax

if (not os.path.exists(os.path.join(NCBITAX, "names.dmp")):
    print("downloading taxonomy to", NCBITAX, file=sys.stderr)
    taxdb = taxopy.TaxDb(taxdb_dir = NCBITAX, keep_files=True)
else:
    print("using taxonomy in", NCBITAX, file=sys.stderr)
    taxdb = taxopy.TaxDb(nodes_dmp = os.path.join(NCBITAX, "nodes.dmp"), names_dmp = os.path.join(NCBITAX, "names.dmp"),
                         merged_dmp = os.path.join(NCBITAX, "merged.dmp"), keep_files=True)

## Bacteroides sp. 3_1_33FAA
## read in external taxonomy: mapping of locus tags to taxid
locus2taxid = pd.read_csv(args.locus2taxid_file,sep='\t', usecols = ["taxid", "pred_gene", "uniref", "class"])
taxids = locus2taxid.taxid.unique()

## get the taxonomy string from the taxid
def taxstring(taxid):
    try:
        ttt = taxopy.Taxon(taxid, taxdb)
    except:
        print(taxid, file=sys.stderr)
        return("unclassified")
    if 'species' in ttt.rank_name_dictionary:
        species = ttt.rank_name_dictionary['species'].replace('[', '').replace(']', '')
        if args.nosp:
            species = re.sub(" sp\\. .+$", " sp.", species)
        if 'genus' in ttt.rank_name_dictionary:
            genus = 'g__' + ttt.rank_name_dictionary['genus'].replace(' ', '_').replace('[', '').replace(']', '')
        else:
            genus = 'g__' + species.split()[0]
        species = 's__' + species.replace(' ', '_')
        return('.'.join([genus, species]))
    elif 'genus' in ttt.rank_name_dictionary:
        return('g__' + ttt.rank_name_dictionary['genus'].replace(' ', '_'))
    else:
        return((ttt.rank + '__' + ttt.name).replace(' ', '_'))

taxid2string = { x: taxstring(x) for x in taxids }

locus2taxid['taxstring'] = locus2taxid['taxid'].map(taxid2string)

## filter the merged table to remove duplicates and unclassified
print("grouping and filtering classification to external taxonomy mapping", file = sys.stderr)
print(locus2taxid.shape, file = sys.stderr)
print(locus2taxid.columns, file=sys.stderr)
locus2taxid.drop(labels = ["pred_gene", "taxid"], inplace = True, axis = 1)
locus2taxid = locus2taxid.set_index("class")
locus2taxid = locus2taxid[locus2taxid.taxstring != "unclassified"].drop_duplicates()
print(locus2taxid.shape, file = sys.stderr)
locus2taxid = locus2taxid.groupby("class").filter(lambda x: len(x["taxstring"]) < 2)
print(locus2taxid.shape, file = sys.stderr)


## read in input genefamilies file
gf = pd.read_csv(args.input, sep = '\t')


## replace taxonomy in genefamilies with string from external mapping
def lookup_taxstring(classtring):
    lookup = locus2taxid.loc[classtring]
    return('|'.join([lookup.uniref, lookup.taxstring]))

print("indexing gf", str(gf.shape), file = sys.stderr)
# gf['newgf'] = "unchanged"
v = gf.index[gf['# Gene Family'].str.contains("|", regex = False) & gf['# Gene Family'].isin(locus2taxid.index)]
# chunk = len(v) / args.threads
print("making genefamilies with new taxonomy", len(v), file = sys.stderr)

gf.loc[v, '# Gene Family'] = list(map(lookup_taxstring, gf.loc[v, '# Gene Family']))

# with concurrent.futures.ProcessPoolExecutor(max_workers=args.threads) as executor:
#     gf.loc[v, '# Gene Family'] = list(executor.map(lookup_taxstring, gf.loc[v, '# Gene Family'], chunksize=chunk.__floor__()))
print("writing new genefamilies file", gf_outfile, file = sys.stderr)
gf.to_csv(gf_outfile, sep = '\t', index = False)
