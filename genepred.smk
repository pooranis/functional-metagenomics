"""
To use in your main snakefile:
include: setup.smk
include: genepred.smk

ASSEMBLYFASTA and BAM variables need to be set before including/using this file.  Ways to do this:

1. If you used trim_assemble_map.smk to make the assemblies and bam files, then it will be included by default.

2. Override defaults by specifying the pattern/format of the assembly file names and bamfiles using {sample} wildcard in project config file:
Below is an example::

ASSEMBLYFASTA: "outdir/{sample}_spades.fasta"
BAM: "outdir/{sample}/{sample}.bam"

"""
import sys
from os.path import join as pjoin

#### INPUTS ##########

## check if trim_assemble_map.smk already included
if (('__createtrimdir' not in vars())):
    include: pjoin(config["scriptdir"], "trim_assemble_map.smk")

## use config if specified instead
ASSEMBLYFASTA = config.get('ASSEMBLYFASTA', ASSEMBLYFASTA)
BAM = config.get('BAM', BAM)


######### TARGET OUTPUTS ###################################
prodigal_prefix = "{sample}.prd"
READCOUNTS = pjoin(SUBOUT, "{sample}.count.CDS.tpm.txt")
PRODIGAL = pjoin(SUBOUT, "prodigal", prodigal_prefix)

########### RULES ##################

rule genepred:
    """predict genes with prodigal and count reads with verse"""
    threads: clust_conf["genepred"]["threads"]
    envmodules: *clust_conf["genepred"]["modules"]
    resources: cc = repr(clust_conf["genepred"])

    input: scaffolds = ASSEMBLYFASTA,
           bam = BAM
    output: gff = PRODIGAL+".gff",
            faa = PRODIGAL+".faa",
            fna = PRODIGAL+".fna",
            sco = PRODIGAL+".sco",
            readcounts = READCOUNTS
    params: work = pjoin(TMP, "prodigal"),
            temprc = pjoin(TMP, "prodigal", "{sample}.count"),
            tempcounts = pjoin(TMP,"prodigal", "{sample}.count.CDS.tpm.txt"),
            final = pjoin(SUBOUT, "prodigal"),
            prefix = prodigal_prefix,
            slist = pjoin(TMP, "list.txt"),
            s = "{sample}"
    shell:"""
    ## cleanup failed jobs
    rm -rf {params.final}

    ## on error mv temp to final outputdir
    trap 'mv -v $TMPDIR {params.final}' ERR

    scafpath=$(realpath {input.scaffolds})

    ## seqkit split makes files input.part_001.fasta
    seqkit split2 -p {threads} ${{scafpath}} -O $TMPDIR/
    skbase=$(ls $TMPDIR/*.part_[0-9][0-9][0-9]* | head -n1)
    skext=${{skbase##*.part_[0-9][0-9][0-9].}}
    skbase=`expr "$skbase" : '\(.*.part\)'`

    ## get just the part numbers in list to iterate over
    ls ${{skbase}}_[0-9][0-9][0-9].${{skext}} | perl -nE '$_ =~ /.+\.part_(\d+).+/; say $1' >{params.slist}
    ls $TMPDIR/

    cat {params.slist} | parallel -j {threads} prodigal -i ${{skbase}}_{{}}.${{skext}} \
                          -d $TMPDIR/{{}}.fna -a $TMPDIR/{{}}.faa \
                          -f gff -o $TMPDIR/{{}}.gff \
                          -s $TMPDIR/{{}}.sco \
                          -p meta

    mkdir -p {params.work}
    ## add sample ID to keep track when collating and part number to make sure IDs are distinct across parallel jobs to CDS ID
    cat {params.slist} | parallel -j1 -q sed "s/\tID=/\tID={params.s}{{}}_/" $TMPDIR/{{}}.gff >{params.work}/{params.prefix}.gff
    ## transform headers to only contain ID because humann and other tools don't like space in header; other info is in gff anyway
    cat {params.slist} | parallel -j1 -q seqkit replace -p '^.*?ID=(.+?);.+$' -r '{params.s}{{}}_$1' $TMPDIR/{{}}.fna >{params.work}/{params.prefix}.fna
    cat {params.slist} | parallel -j1 -q seqkit replace -p '^.*?ID=(.+?);.+$' -r '{params.s}{{}}_$1' $TMPDIR/{{}}.faa >{params.work}/{params.prefix}.faa
    cat {params.slist} | parallel -j1 cat $TMPDIR/{{}}.sco >{params.work}/{params.prefix}.sco

    ## read counts
    verse -a {params.work}/{params.prefix}.gff -o {params.temprc} -z 1 -t CDS -g ID -l -T {threads} --ignoreDup {input.bam}
    {config[scriptdir]}/calc_tpm.py {params.temprc}.CDS.txt >{params.tempcounts}
    mv -vf {params.tempcounts} {output.readcounts}

    mv -v {params.work} {params.final}
    """
