#!/usr/bin/env python3

"""
requires humann version >= 3 in PATH/PYTHONPATH
"""

import pandas as pd
import sys
from humann.utilities import filter_based_on_query_coverage
import re
import argparse
import os

## from humann log file these are defaults
# SEARCH MODE
# search mode = uniref90
# nucleotide identity threshold = 0.0
# translated identity threshold = 80.0

# ALIGNMENT SETTINGS
# bowtie2 options = --very-sensitive
# diamond options = --top 1 --outfmt 6
# evalue threshold = 1.0
# prescreen threshold = 0.01
# translated subject coverage threshold = 50.0
# translated query coverage threshold = 90.0
# nucleotide subject coverage threshold = 50.0
# nucleotide query coverage threshold = 90.0

trans_percid = 80
evalue = 1
trans_qcov = 90
nucl_percid = 0
nucl_qcov = 90



parser = argparse.ArgumentParser(description='Make modified humann gene families file using verse read counts.  Optional args should be same as used with humann - defaults are same as humann defaults if you use uniref90 database.')
req = parser.add_argument_group('required arguments')
req.add_argument('-r', '--readcounts', type=str,
                    help='read count file output of verse', required=True)
req.add_argument('-i', '--input', type=str, help='output directory of humann.  should contain inputprefix_humann_temp dir.', required=True)
req.add_argument('-p', '--inputprefix', type=str,
                    help='prefix or "output basename" used with humann.', required=True)
req.add_argument('-o', '--output', type=str, help='output prefix for mod genefamilies file including output dir for output files', required=True)
parser.add_argument('--nucleotide-identity-threshold', type=float,
                    help='identity threshold for nuclotide alignments [default: %(default)s]',
                    default=nucl_percid, dest='nucl_percid')
parser.add_argument('--nucleotide-query-coverage-threshold', type=float,
                    help='query coverage threshold for nucleotide alignments [default: %(default)s]',
                    default=nucl_qcov, dest='nucl_qcov')
parser.add_argument('--evalue', type=float,
                    help='the evalue threshold to use with the translated search [default: %(default)s]',
                    default=evalue, dest='evalue')
parser.add_argument('--translated-identity-threshold', type=float,
                    help='identity threshold for translated alignments [default: %(default)s]',
                    default=trans_percid, dest='trans_percid')
parser.add_argument('--translated-query-coverage-threshold', type=float,
                    help='query coverage threshold for translated alignments [default: %(default)s]',
                    default=trans_qcov, dest='trans_qcov')
parser._action_groups.reverse()
args = parser.parse_args()

## make default/optional args global
a = vars(args)
for k in ['trans_percid', 'evalue', 'trans_qcov', 'nucl_percid', 'nucl_qcov']:
    val = a[k]
    exec(k + '=val')


genefamfile=os.path.join(args.input, args.inputprefix + '_genefamilies.tsv')
diamond=os.path.join(args.input, args.inputprefix + '_humann_temp', args.inputprefix + '_diamond_aligned.tsv')
bowtie=os.path.join(args.input, args.inputprefix + '_humann_temp', args.inputprefix + '_bowtie2_aligned.tsv')
# bowtie="test.tsv"
pred2unirefmap=args.output + '.pred2uniref.map.txt'
modgenefam=args.output + '_genefamilies.tsv'

if (os.path.abspath(genefamfile) == os.path.abspath(modgenefam)):
    print("ERROR: output modified gene family file name", modgenefam, "is same as input file", genefamfile,
          ". Please specify a different output prefix and rerun.", file=sys.stderr)
    sys.exit(1)



gfamunc=set()
gfam2sp = set()
unmapped = None


print("reading gene families file", genefamfile, file=sys.stderr)
with open(genefamfile, 'r') as f:
    header = f.readline().rstrip().split('\t')
    npos = f.tell()
    un = f.readline().rstrip()
    if un.startswith('UNMAPPED'):
        unmapped = un.split('\t')[1]
    else:
        f.seek(npos)

    for line in f:
        l = line.split()
        if "|" not in l[0]:
            continue
        gene,sp = l[0].split('|')
        if sp == "unclassified":
            gfamunc.add(gene)
        else:
            gfam2sp.add(l[0])


## need to parse sequence lengths counts
print("reading in read counts file", args.readcounts, file=sys.stderr)
rc = pd.read_csv(args.readcounts,sep='\t', index_col='gene')
seqlen = rc['length']

## sequences which aligned to translated db
if (len(gfamunc) > 0):
    print("filtering translated alignments in", diamond, "using thresholds: translated % ID -", str(trans_percid), "evalue -", str(evalue), "translated query coverage -", str(trans_qcov), file=sys.stderr)
    trans = pd.read_csv(diamond, sep='\t', header=None)
    trans['gene'] = trans[1].apply(lambda x: x.split('|')[0])
    ## filter on genes that are in input genefamilies.tsv
    trans = trans[trans['gene'].isin(gfamunc)]
    ## filter on % ID & evalue
    trans = trans[trans[2] > trans_percid]
    trans = trans[trans[10] < evalue]
    trans['query'] = trans[0].apply(lambda x: x.split('|')[0])

    ## filter_based_on_query_coverage returns *False* if query coverage is above trans_qcov
    trans['qcov'] = trans.apply(lambda x: filter_based_on_query_coverage(seqlen[x['query']], x[6], x[7], trans_qcov), axis=1)
    trans = trans[~trans['qcov']]
    trans['class'] = trans['gene'].apply(lambda x: x+'|unclassified')
    trans = trans[['query', 'gene', 'class']]
    trans = trans.rename(columns={"query": 'pred_gene', "gene": 'uniref'})
else:
    trans = pd.DataFrame(columns = ['pred_gene', 'uniref', 'class'])


if (len(gfam2sp) > 0):
    ## sequences which aligned in nucleotide search
    print("filtering nucleotide alignments in", bowtie, "using thresholds: nucleotide % ID -", str(nucl_percid), "nucleotide query coverage -", str(nucl_qcov), file=sys.stderr)
    gpatt = re.compile("g__.+$")
    def geneclass(x):
        """
        get unirefgenename|g__genus;s__species string
        """
        subj = x.split('|')
        uniref = subj[2]
        m = re.search(gpatt, subj[1])
        sp = m.group(0)
        return(uniref, uniref + "|" + sp)

    nucl = pd.read_csv(bowtie, sep='\t', header=None)
    nucl['uniref'],nucl['class'] = zip(*nucl[1].map(geneclass))
    ## filter on genes that are in input genefamilies.tsv
    nucl = nucl[nucl['class'].isin(gfam2sp)]
    ## filter on % ID, evalue, and query coverage
    nucl = nucl[nucl[2] > nucl_percid]
    nucl = nucl[nucl[10] < evalue]
    nucl['qcov'] = nucl.apply(lambda x: filter_based_on_query_coverage(seqlen[x[0]], x[6], x[7], nucl_qcov), axis=1)
    nucl = nucl[~nucl['qcov']]
    nucl = nucl[[0, 'uniref', 'class']]
    nucl = nucl.rename(columns={0 : 'pred_gene'})
else:
    nucl = pd.DataFrame(columns = ['pred_gene', 'uniref', 'class'])

## concat translated and nucleotide filtered results
df = pd.concat([nucl, trans])

if df.shape[0] == 0:
    print("no genes aligned to nucleotide or translated database.  Exiting.", file=sys.stderr)
    sys.exit(0)

## calc RPK https://github.com/biobakery/humann#standard-workflow
df = pd.merge(df, rc, how='left', left_on = 'pred_gene', right_index = True)
# df['score'] = 1000 * df['count'] / df['length']
print("writing predicted gene to uniref gene map to", pred2unirefmap, file=sys.stderr)
df.to_csv(pred2unirefmap,sep='\t', index=False)

# calculated number of reads not mapped
if unmapped is not None:
    unmappeddf = rc[~rc.index.isin(df['pred_gene'])]
    unmapped = unmappeddf['rpk'].sum()
## calculate RPK by uniref gene + tax classification
df = df.drop(columns = ['pred_gene', 'tpm'], axis=1)
df = df.groupby(['uniref', 'class'], as_index=False)
df = df.sum()

## calculate RPK by uniref gene only as both seem to be in _genefamilies.tsv
sumdf = df.copy()
sumdf = sumdf.drop('class', axis=1)
sumdf = sumdf.groupby("uniref", as_index=False)
sumdf = sumdf.sum()

## concatenate and sort
df['uniref'] = df['class']
df = df.drop('class', axis=1)
df = pd.concat([df, sumdf])
df = df.sort_values(by=['uniref'])
df = df.drop(['count', 'length'], axis=1)
## add unmapped to top
if unmapped is not None:
    urow = pd.DataFrame({ 'uniref': 'UNMAPPED', 'rpk': unmapped}, index =[0])
    df = pd.concat([urow, df]).reset_index(drop=True)
df = df.rename(columns={"uniref": header[0], "rpk": header[1]})
print("writing modified gene families to", modgenefam, file=sys.stderr)
df.to_csv(modgenefam, sep= '\t', index=False)
