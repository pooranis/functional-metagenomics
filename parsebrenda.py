#!/usr/bin/env python3

import re
import sys


def getvalue(l):
    l = l.strip()
    value = re.sub('^[A-Z]{2}\s*', '', l)
    return(value)


## input file Downloaded from BRENDA
## https://www.brenda-enzymes.org/index.php
## Download link at bottom, BRENDA text file
infile=sys.argv[1]

with open(infile, 'r') as f:
    id = str()
    ec = []
    rn = {}
    sn = {}
    link = {}
    comment = {}
    for line in f:
        if line.startswith('ID	'):
            id = getvalue(line)
            spl = id.split(maxsplit=1)
            id = spl[0]
            if len(spl) > 1:
                comment[id] = spl[1]
            else:
                comment[id] = ''
            ec.append(id)
        elif line.startswith("RN	"):
            rn[id] = getvalue(line)
        elif line.startswith("SN	"):
            sn[id] = getvalue(line)
    
## print table of enzymes, names and links
## header
print('EC\tRecommendedName\tSystematicName\tLink\tComment')
for id in ec:
    link = 'https://www.brenda-enzymes.org/enzyme.php?ecno=' + id
    print(id, rn[id], sn[id],  link,  comment[id], sep='\t')