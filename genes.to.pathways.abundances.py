#!/usr/bin/env python3

"""
This script summarizes abundances of annotation features.  At minimum, it only requires a file mapping from
locus tag/gene_id to some annotation.

However, typically, I use it to also calculate pathway abundances where the pathways are made up of
the annotations and have a higher level hierarchy.  I usually have:
1. file mapping from locus tag to annotation feature (like EC number)
2. coverage file with locus tag and TPM abundances (so already normalized for gene length).
3. map file mapping from pathway id to EC number
4. kegg or other db hierarchy mapping from pathway id to higher level categories
5. limit file is in format of the report output of minpath containing only pathways I want - usually those with
   "minpath 1" for yes! this is in the sample.

The help is very helpful, for me anyway.
genes.to.pathways.abundances.py -h

modified from https://github.com/EnvGen/metagenomics-workshop/blob/master/in-house/genes.to.kronaTable.py
"""


import sys, csv, numpy as np
from contextlib import ExitStack

def ReadLengths(f):
    d = {}
    with open(f) as hin:
        for line in hin:
            line = line.rstrip()
            [g, l] = line.rsplit()
            l = float(l)
            d[g] = l
    return d

def ReadLimits(f):
    """read in file which is "details" output of MinPath - filtered to contain only those pathways for which to
    calculate abundances.  returns dictionary mapping pathway to total number of elements in hierarcy of that pathway.
    Limits file format:
    path 82 any n/a  naive 1  minpath 1  fam0  21  fam-found  20  name  map00550
    path 83 any n/a  naive 1  minpath 1  fam0  45  fam-found  21  name  map00561

    """
    limit = {}
    with open(f) as hin:
        for line in hin:
            line = line.rstrip()
            fields = line.rsplit()
            limit[fields[-1]] = int(fields[9])
    return limit

def ReadMap(f):
    d = {}
    with open(f) as hin:
        hincsv = csv.reader(hin, delimiter = '\t')
        for row in hincsv:
            ann = row[1]
            parent = row[0]
            try: d[ann].append(parent)
            except KeyError: d[ann] = [parent]
    return d

def ReadCoverage(f, lengths):
    d = {}
    try: hin = open(f, 'r')
    except TypeError: return {}
    hincsv = csv.reader(hin, delimiter = '\t')
    for row in hincsv:
        g = row[0]
        try: c = float(row[1])
        except ValueError: continue
        if len(list(lengths.keys()))>0:
            try: l = lengths[g]
            except KeyError: sys.exit("No length found for gene "+g+"\n")
        else: l = 1
        c = float(c)/l
        d[g] = c
    hin.close()
    return d

def ReadHierarchy(f):
    d = {}
    hin = open(f)
    hincsv = csv.reader(hin, delimiter = '\t')
    l = []
    for row in hincsv:
        id = row[0]
        name = row[1]
        try: hiers = row[2:]
        except IndexError: hiers = ["Unknown"]
        if hiers[0] in (None, ''):
            hiers = ["Unknown"]
        d[id] = [name]+hiers
        l.append(len(d[id]))
    hin.close()
    return (max(l), d)

def Calculate(hier_c, operation, limit):
    hier_sdev = {}
    sys.stderr.write(f"Using {operation} for hierarchy operation.\n")
    if operation == "meanoverall":
        if not limit:
            sys.exit("ERROR: Cannot use 'meanoverall' for operation because limit file not specified or empty.\n")
        opfunction = np.sum
    elif operation == "sum": opfunction = np.sum
    elif operation == "mean" or operation == "meanhalf": opfunction = np.mean
    elif operation == "min": opfunction = min
    for hier, l in hier_c.items():
        hier_sdev[hier] = 0.0
        if operation == "meanhalf":
            l.sort()
            l = l[len(l)/2:]
        if operation == "meanoverall":
            ## divide by total number of elements in pathway
            l = [x/limit[hier] for x in l]
        hier_c[hier] = opfunction(l)
        hier_sdev[hier] = np.std(l)
    return (hier_sdev, hier_c)

def CalcAnnotation(annotations, coverage, summaryop, verbose):
    ## Iterate annotations, and sum coverage if available
    ann_c = {}
    if len(list(coverage.keys())) > 0:
        cov = True
        sys.stderr.write(f"Using {summaryop} for gene summary operation.\n")
    else:
        cov = False
        sys.stderr.write("Coverage file not given or empty.  Using count for gene summary operation.\n")
    for annotation, l in annotations.items():
        covlist = []
        if cov:
            if summaryop == "sum": sumfunction = np.sum
            elif summaryop == "mean": sumfunction = np.mean
            for gene in l:
                try: gene_cov = coverage[gene]
                except KeyError: sys.exit("ERROR: Could not find coverage for gene "+str(gene)+". Are you sure you have coverage information?\n")
                covlist.append(gene_cov)
            ann_c[annotation] = sumfunction(covlist)
        else: ann_c[annotation] = len(l) ## if coverage not given count genes
    return(ann_c)

def CalcHierarchy(mapping, ann_c, operation, limit, verbose):
    ## Transfer annotation sums to nearest parent in mapping, if limit is supplied skip parents in limit
    hier_c = {}
    for annotation, count in ann_c.items():
        try: parents = mapping[annotation]
        except KeyError:
            if verbose: sys.stderr.write("WARNING: Could not find hierarchy parent for "+annotation+"\n")
            continue
        for parent in parents:
            if limit and not parent in limit:
                if verbose: sys.stderr.write("Skipping parent "+ parent+"\n")
                continue
            try: hier_c[parent].append(count)
            except KeyError: hier_c[parent] = [count]
    (hier_sdev, hier_c) = Calculate(hier_c, operation, limit)
    return (hier_sdev, hier_c)

def main():
    """run analysis"""
    from argparse import ArgumentParser
    parser = ArgumentParser(description="Summarize abundances/counts of annotated features.", add_help=False)

    rp = parser.add_argument_group("required arguments")
    rp.add_argument("-i", "--infile", type=str, required=True,
        help="REQUIRED: Tab-delimited file with gene_ids in first column and gene_annotations in second column.")

    op = parser.add_argument_group("optional arguments")
    op.add_argument("-h", "--help", action="help", help="show this help message and exit")
    op.add_argument("-v", "--verbose", action="store_true", help="Run in verbose mode")
    op.add_argument("-a", "--annoutfile", type=str,
                        help="Output file of annotation/gene abundances.  Defaults to infile + '.anno.abund.txt'.")
    op.add_argument("-c", "--coverage", type=str,
                    help="File with first column gene_id, second column gene coverage for each gene_id in infile.")
    op.add_argument("-s", "--summaryop", type=str, default="sum", choices = ['sum', 'mean'],
                         help="""With --coverage, how to summarize gene coverage when more than one
                         gene_id/locus tag matches to an annotation - default '%(default)s'.
                         Without coverage file, gene_ids are summarized by counting the number mapping to the annotation.""")
    op.add_argument("-n", "--name", type=str,
        help="Name to assign to sample in header of outfiles. Defaults to name of infile")
    op.add_argument("-L", "--lengthnorm", type=str,
        help="Provide file with lengths for genes to normalize coverage by")


    hp = parser.add_argument_group(title="hierarchy arguments",
                                        description="""To additionally calculate hierarchy (e.g. pathway) abundances,
                                        must specify map and hierarchy files.""")
    hp.add_argument("-m", "--mapfile", type=str,
        help="Tab-delimited file mapping from the nearest parent hierarchy level to each gene_annotation (e.g. pathway to enzyme)")
    hp.add_argument("-H", "--hierarchy", type=str,
        help="Hierarchy file for parent levels. Tab-delimited with first column corresponding to first column of mapfile.")
    hp.add_argument("-l", "--limit", type=str,
        help="Limit calculations to only this list of parent hierarchies. E.g. a list of predicted pathways only - format like output of minpath")
    hp.add_argument("-O", "--operation", type=str, default="mean", choices = ['mean','meanhalf', 'sum', 'meanoverall', 'min'],
                        help="""Specify how to do calculations over elements of parent hierarchy.
                        'meanhalf' means averages will be calculated from the most abundant half of annotations.
                        'meanoverall' (must use with limit) means averages are calculated over all possible elements in parent,
                        not just those present in sample (as with 'mean').
                        Default '%(default)s'.""")
    hp.add_argument("-o", "--outfile", type=str,
        help="Output file with parent hierarchy abundances.  Defaults to infile + '.pwy.abund.txt'.")
    hp.add_argument("--singlecol", action="store_true",
        help="Write only one annotation column for the first parent hierarchy (e.g. pathway)")
    hp.add_argument("-g", "--missing", type=str, default="missing_pathways.txt",
                        help="File name for missing pathways in hierarchy file - default %(default)s")
    hp.add_argument("--sdev", type=str,
        help="Write the standard deviation of each first parent hierarchy level to this file")

    args = parser.parse_args()

    ## Check arguments
    if (args.mapfile == None) is not (args.hierarchy == None):
        parser.error("Only one of map file or hierarchy file is specified.  Did you forget the other one?")
    elif args.operation == "meanoverall" and not args.limit:
        parser.error("'meanoverall' operation requires limit file.")
    elif args.lengthnorm and not args.coverage:
        parser.error("lengthnorm file given, but no coverage file.")

    verbose = args.verbose


    ## read gene lengths,if needed
    if args.lengthnorm: lengths = ReadLengths(args.lengthnorm)
    else: lengths = {}

    ## Read annotations, then get length of the list for counts
    annotations = ReadMap(args.infile)
    coverage = ReadCoverage(args.coverage, lengths)

    ## Iterate annotations, and sum coverage if available
    anno_abund = CalcAnnotation(annotations, coverage, args.summaryop, verbose)


    ## Set name for sample, if not specified use the basename of the input file
    if args.name: name = args.name
    else: name = (args.infile).split("/")[-1]

    ## Save annotation abundances to file.
    if not args.annoutfile: args.annoutfile = args.infile + '.anno.abund.txt'
    with open(args.annoutfile , 'w') as aout:
        sys.stderr.write('Saving annotation abundances to ' + args.annoutfile + '\n')
        aoutcsv = csv.writer(aout, delimiter = '\t')
        ## header
        aoutcsv.writerow(['X', name])
        for annotation, abund in anno_abund.items():
            aoutcsv.writerow([annotation, abund])

    if not args.mapfile:
        if verbose:
            sys.stderr.write("Mapping and Hierarchy files not given, so only annotation abundances were calculated. Exiting.\n")
        sys.exit(0)

    ##### Hierarchy/pathway
    ## Read the mapping of hierarchies
    mapping = ReadMap(args.mapfile)


    ## read limit file
    if args.limit: limit = ReadLimits(args.limit)
    else: limit = []

    ## calculate hierarchy abundances
    (max_hier, hierarchy) = ReadHierarchy(args.hierarchy)
    (hier_sdev, hier_counts) = CalcHierarchy(mapping, anno_abund, args.operation, limit, args.verbose)

    ## Write output
    if not args.outfile: args.outfile = args.infile + '.pwy.abund.txt'
    with open(args.outfile, 'w') as hout, open(args.missing, "w") as err_f, open(args.sdev, 'w') if args.sdev else ExitStack() as sdevout:
        sys.stderr.write("Saving hierarchy abundances to " + args.outfile + "\n")
        houtcsv = csv.writer(hout, delimiter = '\t')

        ## header
        out = [name, "id"]
        if args.singlecol: out.insert(0, "X")
        else:
            for i in range(max_hier, 0, -1): out.append("Level"+str(i))
        houtcsv.writerow(out)

        if args.sdev:
            sdevout.write("X.sdev\t"+name+"\n")

        for hier, count in hier_counts.items():
            out = [count]
            try: h = hierarchy[hier]
            except KeyError: h = ["Unknown"]
            h.insert(0, hier)
            if args.singlecol: out.insert(0, hier)
            else:
                try:
                    out+=h
                except KeyError:
                    err_f.write("%s\n" % hier)
            houtcsv.writerow(out)
            if args.sdev:
                try: sdevout.write(hier+"\t"+str(hier_sdev[hier])+"\n")
                except NameError: pass

if __name__ == "__main__": main()
