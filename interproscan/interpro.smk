import sys
import re
import os
from os.path import join as pjoin

shell.prefix("set -ve;")


### working directory ###
## for this pipeline inputs and outputs are relative to same working directory
DIR=config["workingdir"]
workdir: config["workingdir"]

### inputs ###
# Expects working dir to contain individual directories for all samples.  Each directory contains
# a contig fasta file and a bam file
SAMPLES = list(config["samples"].keys())


if SAMPLES is None:
    sys.exit("Sample names and input files are needed.")


print(SAMPLES)

def FASTA(wildcards):
    s = wildcards.sample
    return pjoin(s, config["samples"][s] )

### outputs ###
## target outputs
OUTDIR = pjoin("{sample}", "interproscan")
CHUNKDIR = pjoin(OUTDIR, "chunks")
XML = pjoin(OUTDIR, "{sample}.ips.xml")
ALLXML = expand(XML, sample=SAMPLES)
GFF = expand(pjoin("{sample}", "interproscan", "{sample}.ips.gff3"), sample=SAMPLES)
TSV = expand(pjoin("{sample}", "interproscan", "{sample}.ips.tsv"), sample=SAMPLES)
HTML = expand(pjoin("{sample}", "interproscan", "{sample}.ips.html.tar.gz"), sample=SAMPLES)

### rules ###
localrules: all, chunk, concat


rule all:
    input: ALLXML

rule chunk:
    input: FASTA
    output: dynamic(pjoin(CHUNKDIR, "{chunk}" + ".faa"))
    params: outdir=CHUNKDIR, chunksize=config["chunksize"]
    shell:"""

    mkdir -p {params.outdir}

    awk 'BEGIN {{n_seq=0; ch=1;}} /^>/ {{if(n_seq%{params.chunksize}==0){{file=sprintf("{params.outdir}/%d.faa",ch); ch++;}} print >> file; n_seq++; next;}} {{ print >> file; }}' < {input}


    """

rule interpro:
    input: pjoin(CHUNKDIR, "{chunk}" + ".faa")
    output: xml = pjoin(CHUNKDIR, "{chunk}" + ".ips.xml")
    params: outdir=CHUNKDIR,
            tempdir=pjoin(OUTDIR, "temp"),
            base=pjoin(CHUNKDIR, "{chunk}" + ".ips")
    shell:"""

    mkdir -p {params.tempdir}

    let c=${{SLURM_JOB_CPUS_PER_NODE}}-2
    echo ${{c}}

    ./interproscan.sh -appl Hamap,TIGRFAM,Pfam,ProSiteProfiles,SMART,Phobius -cpu ${{c}} -dra -dp -pa  -goterms -i {input} -T {params.tempdir} -f XML -b {params.base} --highmem

    """

rule concat:
    input: dynamic(pjoin(CHUNKDIR, "{chunk}" + ".ips.xml"))
    output: XML
    shell:"""

    cat {input} > {output}

    """
