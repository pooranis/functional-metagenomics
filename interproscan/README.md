# InterProScan on Biowulf

Files to run InterProScan on Biowulf

- [run.sm_interpro.sh](run.sm_interpro.sh) - submit script
- [interpro.smk](interpro.smk) - snakefile
- [cluster.interpro.yaml](cluster.interpro.yaml) - cluster config
- [interpro_config.yaml](interpro_config.yaml) - samples config
- [interproscan.sh](interproscan.sh) - replacement for Biowulf module script *~/myprogs/interproscan.sh*
- [interproscan.properties](interproscan.properties) - properties script which turns on signalP, tmhmm, and phobius. Should go in directory *~/.interproscan-5/* to override module properties.



Biowulf does uses swarm which I find difficult to control and is tied too closely to their architecture, so I prefer to run with snakemake.  [Helpful comment in InterProScan issue](https://github.com/ebi-pf-team/interproscan/issues/3#issuecomment-218535397).  