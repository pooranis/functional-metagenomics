#!/usr/bin/env python3

from lxml import etree
import sys

def fast_iter(context, func, *args, **kwargs):
    """
    https://stackoverflow.com/a/12161078
    http://lxml.de/parsing.html#modifying-the-tree
    Based on Liza Daly's fast_iter
    http://www.ibm.com/developerworks/xml/library/x-hiperfparse/
    See also http://effbot.org/zone/element-iterparse.htm
    https://stackoverflow.com/a/25135314 - useful when searching for rarer tags
    """
    for event, elem in context:
        func(elem, *args, **kwargs)
        elem.clear()
        for ancestor in elem.xpath('ancestor-or-self::*'):
            while ancestor.getprevious() is not None:
                del ancestor.getparent()[0]
    del context


def stdout():
    """
    From lxml test code: https://github.com/lxml/lxml/blob/71667f9ac7694216ee8e793192bcd0993a0cdc66/src/lxml/tests/selftest2.py#L17
    Otherwise xmlfile will try to write a binary stream to stdout (but oddly, if
    we write to file instead in xmlfile, it will work beautifully??)
    """
    if sys.version_info[0] < 3:
        return sys.stdout
    class bytes_stdout(object):
        def write(self, data):
            if isinstance(data, bytes):
                data = data.decode('ISO8859-1')
            sys.stdout.write(data)
    return bytes_stdout()


def process_elem(elem, xfile, nspace):
    m = next(elem.iterchildren('{*}matches'))
    if len(m) > 0:
        etree.strip_elements(elem, '{*}phobius-match')
        m = next(elem.iterchildren('{*}matches'))
        if len(m) > 0:
            ## need to remove namespace since I want to save mem and not append to the xf tree
            ## maybe could use this instead to save time https://stackoverflow.com/a/6546229
            # outstr = etree.tostring(elem, encoding='unicode')
            # outstr = outstr.replace(' xmlns="' + nspace + '"', '')
            # outstr = etree.XML(outstr)
            xfile.write(outstr, pretty_print=True) 

## input file
infile=sys.argv[1]
## namespace of xml file - see beginning if input file for something like xmlns=
ns='http://www.ebi.ac.uk/interpro/resources/schemas/interproscan5'
interProScanVersion="5.42-78.0"

with open(infile, 'rb') as f:
    firstline = f.readline()

toptree = etree.fromstring(firstline)
print(firstline)

## hack to remove namespace from subtrees
# with open(infile, 'rb') as f, etree.xmlfile(stdout(), encoding='utf-8') as xf:
#     f.readline()
#     xf.write_declaration()
#     # generate an element (the root element)
#     nsmap = { None: ns}
#     attrib = { 'interProScanVersion' : interProScanVersion}
#     with xf.element('protein-matches', nsmap=nsmap, attrib = attrib):
#         context = etree.iterparse(f, tag='protein', events = ('end', ), remove_blank_text=True) 
#         fast_iter(context, process_elem, xf, ns)
