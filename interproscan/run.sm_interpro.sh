#!/bin/bash
#SBATCH --job-name=sm_interpro
#SBATCH --output=%x-%j.out
#SBATCH --partition=norm
#SBATCH --mail-type=ALL          # Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --cpus-per-task=2             # CPUs
#SBATCH --mem=2g                        # Job memory request
#SBATCH --time=24:00:00                       # Walltime limit days-hrs:min:sec

module load snakemake/5.19.3 || exit 1
snakebin=$(dirname $(realpath $(which snakemake)))
export PATH=${snakebin}:${PATH}

dir=${HOME}/myprogs

workdir=/scratch/subramanianp4/Hall_MetaErg

configfile=${workdir}/interpro_config.yaml
clusterfile=${dir}/cluster.interpro.yaml

snakemake -s ${dir}/interpro.smk --cluster-config "${clusterfile}" --configfile "${configfile}" \
	  --cluster "sbatch --mem-per-cpu={cluster.h_vmem} --cpus-per-task={cluster.threads} -o {cluster.log}/%x-%j.out --partition={cluster.partition} --time={cluster.walltime} --mail-type={cluster.mail_type}" \
	  --jobname "{name}.{jobid}" --jobs 10 all
