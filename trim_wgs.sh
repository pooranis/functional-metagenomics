#!/bin/bash

dir=$(realpath $(dirname $0))

docopts=$HOME/git/utilities/docopts

if [ ! -e "$HOME/git/utilities/docopts" ]
then
    docopts=${dir}/docopts
fi

eval "$(${docopts} -b -h - : "$@" <<EOF
Usage: $0 [options] <prefix>
Options:
   -q <qualityscore>   minimum avg quality [default: 15]
   -o <outputdir>      output directory [default: $PWD]
   -k                  keep intermediate output
   -t <threads>	       number of threads [default: 10]

EOF
)"

echo "
$0 $@
"


ref=~/.local/bbmap/resources/adapters_plus_flex.fa
klen=25
mink=11
copyundefined=t

outputdir=$(realpath ${outputdir})
inputprefix=${prefix%_R1_001.fastq.gz}
prefix=$(basename ${inputprefix})
prefix=${prefix%_S*}
prefix="${outputdir}/${prefix}"


## Trim both
echo "Right Adapter Trim"
bbduk.sh in=${inputprefix}_R1_001.fastq.gz in2=${inputprefix}_R2_001.fastq.gz out=${prefix}_R1_trim.1.fastq out2=${prefix}_R2_trim.1.fastq k=${klen} mink=${mink} hdist=1 ktrim=r ref=${ref} copyundefined=${copyundefined} minoverlap=10 minlen=60 overwrite=t threads=${threads} stats=${prefix}.bbduk.r1.stats statscolumns=5
echo ""

echo "Right Adapter Trim Stats"
sed -i "s|$outputdir/||g" ${prefix}.bbduk.r1.stats
align -j s -g 2 -a d -s t ${prefix}.bbduk.r1.stats

echo ""
echo "Left Adapter and Quality Trim"
bbduk.sh in=${prefix}_R1_trim.1.fastq in2=${prefix}_R2_trim.1.fastq out=${prefix}_R1_trim.fastq.gz out2=${prefix}_R2_trim.fastq.gz k=${klen} mink=${mink} hdist=1 ktrim=l ref=${ref} copyundefined=${copyundefined} minoverlap=10 qtrim=rl trimq=${q} maq=2 minlen=60 overwrite=t threads=${threads} stats=${prefix}.bbduk.r2.stats statscolumns=5
echo ""

echo "Left Adapter Trim Stats" >>${prefix}.bbduk.log
sed -i "s|$outputdir/||g" ${prefix}.bbduk.r2.stats
align -j s -g 2 -a d -s t ${prefix}.bbduk.r2.stats


if [ ${k} == "false" ]; then
    rm ${prefix}.bbduk.*.stats
    rm ${prefix}_R*_trim.1.fastq
fi
