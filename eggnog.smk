"""
to use add to main snakefile:
## need setup to read in config files, and set up output directories, and shell prefix
include: pjoin(config["scriptdir"], "setup.smk")
include: pjoin(config["scriptdir"], "eggnog.smk")

To override default variables for FAA and GFF (produced by genepred.smk),
include in config file::

## pattern for FAA protein gene sequences with {sample} wildcard
FAA: "outdir/{sample}.prodigal.faa"
## pattern for GFF file with {sample} wildcard
GFF: "outdir/{sample}.prodigal.gff"

"""


from os.path import join as pjoin

######## INPUTS ##########

## check if genepred.smk already included
if (('__genepred' not in vars())):
    include: pjoin(config["scriptdir"], "genepred.smk")

## use config if specified instead
FAA = config.get('FAA', PRODIGAL+'.faa')
GFF = config.get('GFF', PRODIGAL+'.gff')
sample2gff = [ s + "," + GFF.format(sample=s) for s in SAMPLES ]


###### TARGET OUTPUTS ################
EGGNOG = pjoin(LOGS, "eggnog_done.txt")

rule em_align:
    """run eggnog-mapper on translated protein sequences from prodigal"""
    threads: clust_conf["em_align"]["threads"]
    envmodules: clust_conf["em_align"]["modules"]
    resources: cc = repr(clust_conf["em_align"])

    input: faa = ancient(FAA)
    output: orthologs = pjoin(SUBOUT, "eggnog", "{sample}.emapper.seed_orthologs")

    params: work = pjoin(TMP, "eggnog"),
            final = pjoin(SUBOUT, "eggnog"),
            s = "{sample}",
            tmp = pjoin(TMP, "tmp")
    shell:"""
    mkdir -p {params.final}
    mkdir -p {params.work}
    mkdir -p {params.tmp}


    ## on exit mv work files to final outputdir
    trap 'mv -vf {params.work}/* {params.final}/' EXIT

    emapper.py -i {input.faa} -o {params.s} --output_dir {params.work} --cpu {threads} --itype proteins \
        --block_size 3 --index_chunks 2 --override --temp_dir {params.tmp} --no_annot

    """



rule em_annot:
    """run eggnog-mapper annotation on eggnog orthologs output of alignment"""
    threads: clust_conf["em_annot"]["threads"]
    envmodules: clust_conf["em_annot"]["modules"]
    resources: cc = repr(clust_conf["em_annot"])

    input: orthologs = ancient(expand(rules.em_align.output.orthologs, sample=SAMPLES)),
           gff = expand(GFF, sample=SAMPLES),
           fake = ancient(rules.createlogdir.output)
    output: EGGNOG
    params: work = pjoin(TMP, "eggnog"),
            indir = OUT,
            shmdir = clust_conf["em_annot"]["shmdir"],
            parjobs = clust_conf["em_annot"]["parjobs"],
            failed = pjoin(LOGS, "failed.em_annot.log"),
            samples = sample2gff

    shell:"""
    date >>{params.failed}
    ## run eggnog-mapper annotation on eggnog orthologs output of alignment
    ## copy database to /dev/shm
    mkdir -p {params.shmdir}
    trap 'rm -rvf {params.shmdir}' EXIT ERR SIGTERM
    echo 'copying eggnog database to /dev/shm'
    free -h --wide
    cp $EGGNOG_DATA_DIR/eggnog.db {params.shmdir}/
    cp $EGGNOG_DATA_DIR/eggnog_proteins.dmnd {params.shmdir}/
    free -h --wide

    export PROCS=$(echo "{threads} / {params.parjobs}" | bc)
    echo $PROCS
    mkdir -p {params.work}

    runeggnog () {{
      ## turn off -e so if one sample fails, it keeps going
      set +e

      indir={config[workdir]}/$(realpath --relative-to={config[workdir]} {params.indir}/${{1}})
      gff={config[workdir]}/$(realpath --relative-to={config[workdir]} ${{2}})
      emapper.py --annotate_hits_table ${{indir}}/eggnog/${{1}}.emapper.seed_orthologs -o ${{1}} \
              --output_dir {params.work} --cpu $PROCS \
              --override --temp_dir $TMPDIR --data_dir {params.shmdir} \
              --decorate_gff ${{gff}} --decorate_gff_ID_field ID \
              2>&1 | tee ${{indir}}/em_annot.${{1}}.log

      exitcode=$?
      if [[ ${{exitcode}} -ne 0 ]]; then
         echo ${{1}} >>{params.failed}
      fi

      mv -vf {params.work}/${{1}}.emapper.* ${{indir}}/eggnog/

    }}
    export -f runeggnog


    parallel -j {params.parjobs} -C$"\," --tagstring {{1}} runeggnog ::: {params.samples}
    touch {output}
    ### failed samples will be in {params.failed}
    """
