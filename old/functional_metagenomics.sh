#!/bin/bash


## Some helper functions
ecmd () {
    ## for loading modules otherwise weird stuff gets output with set -v or -x
    { eval "$@"; } 2>/dev/null
}

INFO () {
    ## for running echo
    { set +v; } 2>/dev/null
}

INFOdone () {
    ## turn -v back on after echo
    { set -v; } 2>/dev/null
}

TRACE () {
    ## use -x instead of -v like when no comments
    { set +v -x; } 2>/dev/null
}

TRACEdone () {
    ## turn off -x
    { set -v +x; } 2>/dev/null
}

set -ve

## $1 is prefix of filename of FASTQ file assumed to have format ${1}_R1_001.fastq.gz
## it's assumed the script is being run in the output directory; if not then add cd outdir
## statement to beginning.

eval "prefix=${1}"


## We kind of follow this workflow: https://metagenomics-workshop.readthedocs.io/en/latest/annotation/index.html

## Main directory
dir=/hpcdata/lpd_mbc/Data_Analysis/mosaic_samples/niaid_analysis

## Input directory that has raw FASTQ files
eval inputdir=${dir}/input

date

## Trim using BBDuk
## trim_wgs.sh: https://github.niaid.nih.gov/subramanianp4/utilities/blob/master/functional/trim_wgs.sh
${inputdir}/trim_wgs.sh -o ${inputdir} ${inputdir}/${prefix}
#### Trimming done ####

date

## Belkaid Lab sequence file prefixes have sample name plus a sample number appended for the run.
## So for cleanness of output we will remove that final sample number.
eval sample=${prefix%_S*}

#### Metagenomics assembly
ecmd module load SPAdes/3.13.0-foss-2016b

metaspades.py -o ${sample} -1 ${inputdir}/${sample}_R1_trim.fastq.gz -2 ${inputdir}/${sample}_R2_trim.fastq.gz --phred-offset 33

ecmd mpurge


#### Map reads back to assembly
date

TRACE
ecmd module load Bowtie2/2.3.4.1

bowtie2-build --verbose --threads 13 ${sample}/contigs.fasta ${sample}/${sample} | sed -e '/Entering Ebwt loop/,/Exited Ebwt loop/{//!d}'

date

bowtie2 --sensitive-local --no-unal -S ${sample}.bowtie2.sam -p 16 -t -x ${sample}/${sample} -1 ${dir}/input/${sample}_R1_trim.fastq.gz -2 ${dir}/input/${sample}_R2_trim.fastq.gz

ecmd mpurge

date

ecmd module load SAMtools/1.9-goolf-1.7.20

samtools sort --threads 10 -O BAM -o ${sample}.bowtie2.bam ${sample}.bowtie2.sam

ecmd mpurge
date
TRACEdone


## remove duplicates from bamfile
## using picard script: https://github.niaid.nih.gov/subramanianp4/utilities/blob/master/picard
TRACE
ecmd module load picard/2.18.27

picard MarkDuplicates I=${sample}.bowtie2.bam O=${sample}.bowtie2.markdup.bam REMOVE_DUPLICATES=true M=${sample}.markdup.metrics AS=true VALIDATION_STRINGENCY=LENIENT

ecmd mpurge


rm ${sample}.bowtie2.sam
mv ${sample}.bowtie2.bam ${sample}
mv ${sample}.markdup.metrics ${sample}
TRACEdone

INFO
echo "${sample}.bowtie2.sam (output of bowtie2) deleted."
echo "Sorted bam file ${sample}.bowtie2.bam (output of samtools) and ${sample}.markdup.metrics moved to ${sample}."
echo "${sample}.bowtie2.markdup.bam is sorted bamfile with duplicates removed (output of picard MarkDuplicates).
It is the final output of mapping the reads to the assembly."
INFOdone

#### Annotate assembly with prokka
ecmd module load Prokka/1.13

## shortening contig names bc prokka needs names shorter than 37 char
eval outdir=${PWD}/${sample}

outfile=${outdir}/contigs.prokka.fasta
export outfile

perl -nE 'BEGIN {open $OUTFILE, ">$ENV{outfile}";} if ($_ =~ /^>(NODE_\d+)_(.+)/) { say $1 . " " . $2 . " " . $1 . "_" . $2; say $OUTFILE ">$1";} else { print $OUTFILE $_; } END { close $OUTFILE; }' ${sample}/contigs.fasta >${sample}/contigs.headermap.txt


INFO
echo "${sample}/contigs.prokka.fasta contains renamed contigs"
echo "${sample}/contigs.headermap.txt is mapping from short contig names to real ones"
INFOdone

# modified prokka script: https://github.niaid.nih.gov/subramanianp4/utilities/blob/master/functional/prokkapoorani
prokkapoorani --cpus 0 --addgenes --mincontiglen 200 --metagenome --skipgbk --force --prefix ${sample}.prokka --outdir ${sample}/prokka ${sample}/contigs.prokka.fasta



